"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Config {
    constructor() {
        let address, ifaces = require('os').networkInterfaces();
        console.log(ifaces);
        for (let dev in ifaces) {
            ifaces[dev].filter((details) => details.family === 'IPv4' && details.internal === false ? address = details.address : undefined);
        }
        console.log(address);
    }
}
exports.Config = Config;
//# sourceMappingURL=Config.js.map