"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
const fs = require("fs");
class Speech {
    constructor() {
        this.watsonCredentials = { user: '', password: '' };
    }
    setWatson(user, password) {
        this.watsonCredentials.user = user;
        this.watsonCredentials.password = password;
    }
    //MUST be wav file
    AudioToText(filePath, callback) {
        if (!fs.existsSync(filePath)) {
            console.log('Error in AudioToText, file does not exist ', filePath);
            callback('Error in AudioToText, file does not exist ' + filePath, null);
            return;
        }
        const speech_to_text = new SpeechToTextV1({
            'username': this.watsonCredentials.user,
            'password': this.watsonCredentials.password
        });
        const params = {
            // From file 
            audio: fs.createReadStream(filePath),
            content_type: 'audio/l16; rate=44100'
        };
        speech_to_text.recognize(params, (err, res) => {
            if (err) {
                console.log(err);
                callback(err, null);
                return;
            }
            else {
                const result = JSON.stringify(res, null, 2);
                callback(null, result);
            }
        });
    }
}
exports.Speech = Speech;
//# sourceMappingURL=Speech.js.map