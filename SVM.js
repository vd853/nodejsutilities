"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SVMLib = require("libsvm-js/asm");
class SVM {
    constructor(gamma, cost) {
        this.gamma = gamma;
        this.cost = cost;
        this.input = [];
        this.output = [];
        this.hasRun = false;
        const options = {
            kernel: SVMLib.KERNEL_TYPES.RBF,
            type: SVMLib.SVM_TYPES.EPSILON_SVR,
            gamma: gamma,
            cost: cost
        };
        this.svm = new SVMLib(options);
    }
    Start() {
        if (!this.svm) {
            throw 'No svm class created';
        }
        this.svm.train(this.input, this.output);
        this.hasRun = true;
    }
    pushTrainingSet(input, output) {
        this.input.push(input);
        this.output.push(output);
    }
    predict(input) {
        return this.svm.predictOne(input);
    }
}
exports.SVM = SVM;
//# sourceMappingURL=SVM.js.map