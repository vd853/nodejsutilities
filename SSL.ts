import {exec} from 'child_process';
import {File} from './File';
import * as fs from 'fs';
import { ExpressObj, SSLConfig } from './Webserver';

export class SSL{

    //openSSLBinPath is only use for windows
    //for linux, install it, and test from ssh that "openssl" will call openssl
    public static validatePEM(openSSLBinPath: string, pemFile: string, keyPassphrase: string, isKey: boolean, callback:(err, result)=>void){
        
        let path = File.reparsePath(openSSLBinPath) + '/openssl'
        path = process.platform === "win32"? path : 'openssl'
        if(process.platform === "win32"){
            if(!fs.existsSync(path + '.exe')) {callback(('openssl not found: ' + path + '.exe'), null); return;}
        }

        const file = File.reparsePath(pemFile)
        if(!fs.existsSync(file)) {callback('pem file not found', null); return;}
        if(isKey){
            exec(path + ' rsa -inform PEM -in ' + file + ' -passin pass:' + keyPassphrase, (e,s)=>{
                if(e){
                    callback(e, null);
                    return;
                }
                const reg1 = new RegExp('-----END RSA PRIVATE KEY-----')
                if(!reg1.test(s)){
                    callback(null, false);
                    return;
                }
                callback(null, true);
            })
        }else{
            exec(path + ' x509 -inform PEM -in ' + file + ' -passin pass:' + keyPassphrase, (e,s)=>{
                if(e){
                    callback(e, null);
                    return;
                }
                const reg2 = new RegExp('-----END CERTIFICATE-----')
                if(!reg2.test(s)){
                    callback(null, false);
                    return;
                }
                callback(null, true);
            })
        }
    }

    // public static validatePEM2(pemFile: string, passphrase: string, callback:(err, result)=>void){
    //     const file = File.reparsePath(pemFile)
    //     if(!fs.existsSync(file)) {callback('pem file not found', null); return;}
    //     const ssl = new SSLConfig();
        
    //     const ex = new ExpressObj(7777, undefined, undefined, undefined, undefined)
    // }
}