import * as SVMLib from 'libsvm-js/asm'
export class SVM{
    svm: SVMLib
    input: Array<Array<number>> = []
    output: Array<number> = []
    hasRun = false;
    constructor(private gamma: number, private cost: number)
    {
        const options = {
            kernel: SVMLib.KERNEL_TYPES.RBF, 
            type: SVMLib.SVM_TYPES.EPSILON_SVR,    
            gamma: gamma,                    
            cost: cost
        }
        this.svm = new SVMLib(options)
    }
    Start(){
        if(!this.svm){
            throw 'No svm class created'
        }
        this.svm.train(this.input, this.output)
        this.hasRun = true;
    }
    pushTrainingSet(input: Array<number>, output: number){        
        this.input.push(input)
        this.output.push(output)
        
    }
    predict(input: Array<number>):number{
        return this.svm.predictOne(input)
    }
}