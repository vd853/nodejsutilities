//This class is use to extend any model controllers that will use sequelize
//Things to update Interface, concrete, Objectify, sequelize model, print

import {EventEmitter} from 'events';
import * as _ from 'lodash';
export abstract class Entity{
    public Emitter = new EventEmitter();
    constructor(public connection?: any){ //this is optional incase you just want to use it as a data model

    }
    abstract GetModel(); //use to get sequelize model
    // Implement example:
    // public GetModel() { //this must match the interface
    //     return this.connection.define('account',
    //         {
    //             id:{
    //                 type: seq.STRING,
    //                 primaryKey: true,
    //                 allowNull: false
    //             },
    public Add(data: any){
        const b = this.GetModel().build(data); 
        b.save()
        .then(d=>{
            console.log('DB Added')
            if(d)this.Emitter.emit('Add', d)
        })
        .catch(e=>{
            console.log(e)
            if(e)this.Emitter.emit('Add', e)
        })
    }
    public Update(data: any, updater: any) {
        const b = this.GetModel();        
        b.update(data, {
            where: updater
        })
            .then((d) => {
                console.log('DB Updated ', updater)
                if(d)this.Emitter.emit('Update', d);
        })
            .catch((e) => {
                console.log(e)
                if(e)this.Emitter.emit('Update', e);
        });
    }
    //example: {title: 'some'} as argument from your concrete class
    public Delete(deleteObject: any, callback?:(deletedModel:any)=>void){
        const b = this.GetModel();
        b.destroy({
            where:deleteObject
        })
        .then(()=>{
            if(callback)callback(deleteObject)
            this.Emitter.emit('Delete', 'Delete was successful')
        })
        .catch(()=>{
            if(callback)callback(deleteObject)
            this.Emitter.emit('Delete', 'Delete has failed!')
        })
    }
    //example: {title: 'some'} as argument from your concrete class
    public Get(searchObject: any, callback?:(d)=>void) {
        this.GetModel().findOne({
            where: searchObject
        })
            .then(d => {                
                if(!d){
                    console.log('GET HAS NO RETURN VALUE');
                    this.Emitter.emit('Get', null);
                    if(callback)callback(null); //don't string this in a console.log
                    return;
                }
                this.Emitter.emit('Get', d.dataValues);
                if(callback)callback(d.dataValues); //don't string this in a console.log
        })
    }   


    //example: {title: 'some'} or as array [{userId: userId},{profile: profileId}] as argument from your concrete class
    //example of sort order: [['title', 'DESC'], ['peers', 'DESC']]
    //start is being position, amount are items after that position
    //searchObject is required, others can be Undefined
    //use null or undefined if you don't want start, amount, or order
    public GetWhere(searchObject: any, start: number, amount: number, order: Array<Array<string>>, callback?:(d)=>void) {
        let options = {where: searchObject};
        if(order){
            if(order.length){
                options = _.merge(options, {order: order})
            }
        }
        if(start !== null){
            if(amount){
                options = _.merge(options, {offset: start})
                options = _.merge(options, {limit: amount})
            }
        }
        console.log('Getwhere options ', options)
        this.GetModel().findAll(options)
            .then(d => {          
                if(!d){
                    console.log('GETWHERE HAS NO RETURN VALUE')
                    return;
                }      
                const actualValues = [];
                d.forEach(element => {
                    actualValues.push(element.dataValues)   
                });
                this.Emitter.emit('GetWhere', actualValues);  
                if(callback)callback(actualValues); //accessable as an array of dataValues. ex: d[0].dataValues
        })
    }   

    //use this for custom options
    public GetWhereOpt(options: any, callback?:(d)=>void){
        console.log('Getwhere options ', options)
        this.GetModel().findAll(options)
            .then(d => {          
                if(!d){
                    console.log('GETWHERE HAS NO RETURN VALUE')
                    return;
                }      
                const actualValues = [];
                d.forEach(element => {
                    actualValues.push(element.dataValues)   
                });
                this.Emitter.emit('GetWhere', actualValues);  
                if(callback)callback(actualValues); //accessable as an array of dataValues. ex: d[0].dataValues
        })
    }
    //Example your argument could be MyModel, {title: 'some title'}
    //This callback will return the new model, but NOT add or update themselves
    public AddOrUpdate(data: any, updater: any, callback?:(model:any)=>void){
        this.Emitter.on('Get', (c)=>{
            if(!c){
                this.Emitter.on('Add', d=>{
                    this.Emitter.emit('AddOrUpdate', {return: data, method: 'Add'})
                    if(callback)callback(data)
                })
                this.Add(data);
            }else{
                this.Emitter.on('Update', d=>{
                    this.Emitter.emit('AddOrUpdate', {return: data, method: 'Update'})
                    if(callback)callback(data)
                })
                this.Update(data, updater);
            }
        });
        this.Get(updater);
    }
    public GetAll(callback?:(d)=>void){ //does not return dataValues like in the get method
        this.GetModel().findAll().then(
            (e)=>{
                if(callback) callback(e); //accessable as an array of dataValues. ex: e[0].dataValues
                this.Emitter.emit('GetAll', e)
            }
        )
    }

    public GetAllLimited(start: number, amount: number, callback?:(d)=>void){ //does not return dataValues like in the get method
        this.GetModel().findAll({
            offset: start,
            limit: amount
        }).then(
            (e)=>{
                if(callback) callback(e); //accessable as an array of dataValues. ex: e[0].dataValues
                this.Emitter.emit('GetAll', e)
            }
        )
    }

    public GetAllColumn(name: string, callback?:(d)=>void){
        this.GetModel().findAll({
            attributes: [name]
        }).then(
            (e)=>{
                const l = [];
                e.forEach((d)=>{
                    l.push(d.dataValues[name])
                })
                if(callback) callback(l);
                this.Emitter.emit('GetAllColumn', l)
            }
        )
    }
}