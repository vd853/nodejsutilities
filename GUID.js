"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const shortid = require("shortid");
class GUID {
    static create(isShort = false) {
        if (isShort) {
            return shortid.generate();
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
exports.GUID = GUID;
//export{GUID};
//# sourceMappingURL=GUID.js.map