export class Replacer{
    //for dot . use '\\.'
    //This class is fluent
    constructor(private input: string){

    }
    public ReplaceAll(original: string, replacement: string){
        this.input = this.input.replace(new RegExp(original, 'g'), replacement);
        return this;
    }
    public DotWithSpace(){
        this.ReplaceAll('\\.', ' ');
        return this;
    }
    public RemoveExtraSpace(){
        this.input = this.input.replace(/ +(?= )/g,'');
        return this;
    }
    public ToString(){
        return this.input;
    }
    public ReplaceEntire(replacement: string){
        this.input = replacement;
        return this;
    }
    public RemovePunctuation(){
        const puncs = ['\\[', '\\]', '\\{', '\\}', '\\,', '\\;', '\\(', '\\)', '\\-', '\\/', '\\:', '\\!', '\\"', '\'', '\\*', '\\#', '\\@', '\u0027']
        puncs.forEach(e=>{
            this.input = this.ReplaceAll(e, ' ').ToString();
        })
        this.input = this.DotWithSpace().RemoveExtraSpace().ToString().trim();
        return this;
    }
    public ToArray(Lower = false): Array<string>{
        return Replacer.StaticToArray(this.input, Lower);
    }
    public static StaticToArray(text: string, Lower = false, delimiter = ' '): Array<string>{
        if(!text) return [];
        const a = text.split(delimiter);
        for(let i = 0; i < a.length; i++){
            a[i] = a[i].trim();
            if(Lower){
                a[i] = a[i].toLowerCase();
            }
        }
        return a;
    }
}