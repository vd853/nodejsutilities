"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const synaptic = require("synaptic");
const fs = require("fs");
class NeuralNetwork {
    constructor(inputSize, // your raw input and output values should be scaled similarly for better results
    outputSize, hiddenSize, // setting this too high will result in overfitting
    errorReduction = 4, log = true) {
        this.inputSize = inputSize;
        this.outputSize = outputSize;
        this.hiddenSize = hiddenSize;
        this.errorReduction = errorReduction;
        this.log = log;
        this.trainingSet = [];
        this.iterations = Math.pow(10, 6);
        this.scaler = 1; //this can be consider high
        this.error = 1 * Math.pow(10, -errorReduction);
        //hidden neuron size are currently hardcoded
        this.perceptron = new synaptic.Architect.Perceptron(inputSize, hiddenSize, hiddenSize, outputSize); //no need to do more than 2 hidden layer for most situtation
        this.schedule = {
            every: 500,
            do: (data) => {
                if (this.log) {
                    console.log("error", data.error, "iterations", data.iterations, "rate", data.rate);
                }
            }
        };
        this.options = {
            rate: 0.001,
            iterations: this.iterations,
            //shuffle: true,
            error: this.error,
            //cost: synaptic.Trainer.cost.MSE,
            schedule: this.schedule
        };
    }
    pushTrainigSetUnit(input, output) {
        if (input.length !== this.inputSize) {
            throw new Error('You are pushing an input array of the wrong size');
        }
        if (output.length !== this.outputSize) {
            throw new Error('You are pushing an output array of the wrong size');
        }
        //scaler will be use to normal between training and activation
        input.forEach(element => {
            if (element > this.scaler)
                this.scaler = element;
        });
        output.forEach(element => {
            if (element > this.scaler)
                this.scaler = element;
        });
        this.trainingSet.push({ input: input, output: output });
    }
    Start(callback) {
        //console.log('training set ', this.trainingSet)
        //console.log('option ', this.options)
        const trainer = new synaptic.Trainer(this.perceptron);
        this.result = trainer.train(this.scaledData(), this.options);
        callback(this.result);
    }
    scaledData() {
        const trainingSetScaled = [];
        this.trainingSet.forEach(e => {
            const input = [];
            const output = [];
            e.input.forEach(i => {
                input.push(i / this.scaler);
            });
            e.output.forEach(o => {
                output.push(o / this.scaler);
            });
            trainingSetScaled.push({ input: input, output: output });
        });
        if (this.log)
            console.log('scaled training set ', trainingSetScaled);
        return trainingSetScaled;
    }
    save(file, callback) {
        if (this.result) {
            fs.writeFile(file, JSON.stringify(this), callback);
        }
    }
    load(file, callback) {
        fs.readFile(file, 'utf8', (err, data) => {
            if (err) {
                callback(err);
                return;
            }
            else {
                const Jthis = JSON.parse(data);
                this.error = Jthis.error;
                this.inputSize = Jthis.inputSize;
                this.iterations = Jthis.iterations;
                this.log = Jthis.log;
                this.options = Jthis.options;
                this.outputSize = Jthis.outputSize;
                this.result = Jthis.result;
                this.perceptron = synaptic.Architect.Perceptron.fromJSON(Jthis.perceptron);
                callback(this.perceptron);
            }
        });
    }
    activate(input) {
        const eachOneMulti = (array, scale) => {
            const r = [];
            array.forEach(element => {
                r.push(element * scale);
            });
            //console.log('multi ', r)
            return r;
        };
        const eachOneDiv = (array, scale) => {
            const r = [];
            array.forEach(element => {
                r.push(element / scale);
            });
            //console.log('divide ', r)
            return r;
        };
        const result = this.perceptron.activate(eachOneDiv(input, this.scaler));
        return eachOneMulti(result, this.scaler);
        if (input.length === this.inputSize) {
            const result = this.perceptron.activate(eachOneDiv(input, this.scaler));
            return eachOneMulti(result, this.scaler);
        }
        else {
            throw new Error(input.length + ' is the wrong length, correct length is ' + this.inputSize);
        }
    }
}
exports.NeuralNetwork = NeuralNetwork;
//# sourceMappingURL=NeuralNetwork.js.map