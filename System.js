"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const opn = require("opn");
class System {
    //Gets all the argument pass in from the CLI when node runs a js with args, example "node myscript myparam1 myparam2"
    static CaptureCLIArugments() {
        const args = [];
        process.argv.forEach(function (val, index, array) {
            if (index > 1) {
                args.push({ index: index - 2, arg: val });
            }
        });
        return args;
    }
    //can't get the cwd option to work. consider putting cd PATH in the bat or make a bat file generator...
    static Run(file) {
        const b = child_process_1.spawn(file);
        b.stdout.on('data', (d) => {
            console.log('FROM: ' + file + '\n' + d.toString());
        });
    }
    static Open(file) {
        opn(file);
    }
}
exports.System = System;
//# sourceMappingURL=System.js.map