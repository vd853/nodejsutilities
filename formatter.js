"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Replacer {
    //for dot . use '\\.'
    //This class is fluent
    constructor(input) {
        this.input = input;
    }
    ReplaceAll(original, replacement) {
        this.input = this.input.replace(new RegExp(original, 'g'), replacement);
        return this;
    }
    DotWithSpace() {
        this.ReplaceAll('\\.', ' ');
        return this;
    }
    RemoveExtraSpace() {
        this.input = this.input.replace(/ +(?= )/g, '');
        return this;
    }
    ToString() {
        return this.input;
    }
    ReplaceEntire(replacement) {
        this.input = replacement;
        return this;
    }
    RemovePunctuation() {
        const puncs = ['\\[', '\\]', '\\{', '\\}', '\\,', '\\;', '\\(', '\\)', '\\-', '\\/', '\\:', '\\!', '\\"', '\'', '\\*', '\\#', '\\@', '\u0027'];
        puncs.forEach(e => {
            this.input = this.ReplaceAll(e, ' ').ToString();
        });
        this.input = this.DotWithSpace().RemoveExtraSpace().ToString().trim();
        return this;
    }
    ToArray(Lower = false) {
        return Replacer.StaticToArray(this.input, Lower);
    }
    static StaticToArray(text, Lower = false, delimiter = ' ') {
        if (!text)
            return [];
        const a = text.split(delimiter);
        for (let i = 0; i < a.length; i++) {
            a[i] = a[i].trim();
            if (Lower) {
                a[i] = a[i].toLowerCase();
            }
        }
        return a;
    }
}
exports.Replacer = Replacer;
//# sourceMappingURL=formatter.js.map