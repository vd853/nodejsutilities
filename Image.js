"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const resizer = require("resize-img");
const mergeImg = require("merge-img");
const ImageSize = require("image-size");
const fs = require("fs");
const events_1 = require("events");
const imagemin = require("imagemin");
const imageminjpegtran = require("imagemin-jpegtran");
const imageminpngquant = require("imagemin-pngquant");
const File_1 = require("./File");
const base64Img = require("base64-img");
const GUID_1 = require("./GUID");
const async = require("async");
const stat = require("probe-image-size");
const pngStream = require("png-file-stream");
const gifEncoder = require("gifencoder");
const exif = require("fast-exif");
const rotate = require("auto-rotate");
const _ = require("lodash");
const jimp = require("jimp");
const pixelGetter = require("pixel-getter");
class Image {
    constructor(AbsoluteFilePath) {
        this.AbsoluteFilePath = AbsoluteFilePath;
        this.Emit = new events_1.EventEmitter();
        if (!fs.existsSync(AbsoluteFilePath)) {
            console.log('File does not exist ', AbsoluteFilePath);
            throw 'File does not exist ' + AbsoluteFilePath;
        }
    }
    //output is exact file name and full path
    Resize(AbsoluteOutputFilePath, maxWidth, callback) {
        resizer(fs.readFileSync(this.AbsoluteFilePath), { width: maxWidth }).then(buf => {
            fs.writeFile(AbsoluteOutputFilePath, buf, (err) => {
                if (err) {
                    console.log('Resizing error ', err);
                    callback(err, null);
                    throw err;
                }
                callback(null, this.AbsoluteFilePath);
                this.Emit.emit('Resize');
            });
        });
    }
    //output is exact file name and full path
    Compress(AbsoluteOutputFilePath, callback) {
        if (!File_1.File.CopyFile(this.AbsoluteFilePath, AbsoluteOutputFilePath, () => {
            imagemin([AbsoluteOutputFilePath], File_1.File.getPathOnly(AbsoluteOutputFilePath), {
                plugins: [
                    imageminjpegtran(),
                    imageminpngquant()
                ]
            }).then(() => {
                this.Emit.emit('Compress');
                callback(null, AbsoluteOutputFilePath);
            }).catch((err) => {
                callback(err, null);
            });
        })) {
        }
    }
    GeoLocation(callback) {
        exif.read(this.AbsoluteFilePath)
            .then(data => {
            if (!data) {
                callback(null, null);
                return;
            }
            if (!data.gps) {
                callback(null, null);
                return;
            }
            if (data.gps) {
                if (data.gps.GPSLatitude) {
                    let latitude = data.gps.GPSLatitude[0] + data.gps.GPSLatitude[1] / 60 + data.gps.GPSLatitude[2] / 60 / 60;
                    let longitude = data.gps.GPSLongitude[0] + data.gps.GPSLongitude[1] / 60 + data.gps.GPSLongitude[2] / 60 / 60;
                    if (data.gps.GPSLatitudeRef !== 'N') {
                        latitude = -latitude;
                    }
                    if (data.gps.GPSLongitudeRef !== 'E') {
                        longitude = -longitude;
                    }
                    const dataReturn = { latitude: latitude, longitude: longitude };
                    callback(null, dataReturn);
                }
                else {
                    callback(null, null);
                }
            }
            else {
                callback(null, null);
            }
        })
            .catch(err => { if (err)
            callback(err, null); console.log('ERROR GeoLocation'); });
    }
    //AbsoluteOutputFilePath is exact file name and path
    //this is NOT batch
    CompressAndResize(AbsoluteOutputFilePath, maxWidth, callback) {
        const resizer = (err, data) => {
            if (err) {
                console.log('error in compress ', err);
                callback(err, null);
                throw err;
            }
            this.Resize(AbsoluteOutputFilePath, maxWidth, callback);
        };
        // this.Emit.on('Compress', ()=>{
        //     // this.Emit.on('Resize', ()=>{
        //     //     this.Emit.emit('CompressAndResize');
        //     //     callback(null, AbsoluteOutputFilePath);
        //     // })
        // })
        const folder = File_1.File.getPathOnly(AbsoluteOutputFilePath);
        File_1.File.createFolder(folder);
        this.Compress(AbsoluteOutputFilePath, resizer); //goes back to top on completed for resizing
        // fs.exists(folder, exists=>{
        //     if(!exists){
        //         fs.mkdir(folder, (e)=>{
        //             if(e){
        //                 console.log('CompressAndResize error ', e)
        //                 callback(e, null)
        //                 throw e
        //             }
        //         })
        //     }else{
        //     }
        // })
    }
    static exif(path, callback) {
        const ext = File_1.File.getExtension(path);
        if (!_.includes(['jpg', 'JPG', 'png', 'PNG'], ext)) {
            callback('ERROR: Not a jpg file', null);
            return;
        }
        exif.read(path)
            .then(data => {
            callback(null, data);
        })
            .catch(err => callback(err, null));
    }
    //only works for png
    static stats(path, callback) {
        fs.exists(path, exist => {
            if (exist) {
                if (!_.includes(['jpg', 'JPG', 'png', 'PNG'], File_1.File.getExtension(path))) {
                    callback('ERROR Image.stats: only support png files', null);
                    return;
                }
                stat(fs.createReadStream(path))
                    .then(data => {
                    callback(null, data);
                })
                    .catch(err => {
                    callback(err, null);
                });
            }
            else {
                callback('file not exist', null);
            }
        });
    }
    //imagePath must be png, size is 0-4
    //will stamp horizontally for each string
    static textImage(text, imagePath, outImagePath, fontSize = 3, x = 0, y = 0, colorDark = true, callback) {
        let fontSizer = null;
        let horizontalOffset = 32;
        let triggered = false;
        const tasks = [];
        const colorValue = colorDark ? 'BLACK' : 'WHITE';
        switch (fontSize) {
            case 0:
                fontSizer = jimp['FONT_SANS_8_' + colorValue];
                horizontalOffset = 8;
                break;
            case 1:
                fontSizer = jimp['FONT_SANS_16_' + colorValue];
                horizontalOffset = 16;
                break;
            case 2:
                fontSizer = jimp['FONT_SANS_32_' + colorValue];
                break;
            case 3:
                fontSizer = jimp['FONT_SANS_64_' + colorValue];
                horizontalOffset = 64;
                break;
            case 4:
                fontSizer = jimp['FONT_SANS_128_' + colorValue];
                horizontalOffset = 128;
                break;
            default:
                fontSizer = jimp['FONT_SANS_32_' + colorValue];
        }
        const applyOnce = (currentLine, callback) => {
            ImageSize(imagePath, (err, data) => {
                if (err) {
                    console.log('ERROR0 textImage ', err);
                    callback(err, null);
                    return;
                }
                // console.log('stats ', data, err)
                // console.log('message ', currentLine)
                const Jimp = jimp;
                console.log('horizontal ', y);
                Jimp.read(imagePath, (err, image) => {
                    if (err) {
                        console.log('ERROR1 textImage ', err);
                        callback(err, null);
                        return;
                    }
                    Jimp.loadFont(fontSizer).then(f => {
                        image.print(f, x, y, currentLine, data.width);
                        image.quality(100);
                        image.write(outImagePath, (error, data) => {
                            if (error) {
                                callback(error, null);
                                return;
                            }
                            y += horizontalOffset;
                            if (triggered) {
                                triggered = true;
                                imagePath = outImagePath;
                            }
                            //func success callback
                            callback(null, data);
                        });
                    }).catch(e => {
                        console.log('ERROR2 textImage ', e);
                        callback(null, e);
                    });
                });
            });
        };
        text.forEach(e => {
            tasks.push(async.apply(applyOnce, e));
        });
        async.series(tasks, (err, result) => {
            if (err) {
                callback(err, null);
                return;
            }
            callback(err, result);
        });
    }
    //warning this will remove exif data
    static autoRotate(input, output, callback) {
        rotate.autoRotateFile(input, output)
            .then(function (rotated) {
            console.log(rotated ? 'Image rotated' : 'No rotation was needed');
            callback(null, rotated);
        }).catch(function (err) {
            console.error('Got error from auto-rotation: ' + err);
            callback(err, null);
        });
    }
    static rotate(input, output, degress, callback) {
        jimp.read(input, (err, image) => {
            if (err) {
                callback(err, null);
                return;
            }
            image.rotate(degress);
            image.quality(100);
            image.write(output, (error, data) => {
                if (error) {
                    callback(error, null);
                    return;
                }
                //func success callback
                callback(null, true);
            });
        });
    }
    //takes images and merge from left to right, use other method to resize them
    static imageMerge(imagesPath, output, callback) {
        const option = { color: 0x000000FF, align: 'center' };
        imagesPath.forEach(e => {
            if (!fs.existsSync(e)) {
                callback('file not exist ' + e, null);
                return;
            }
        });
        mergeImg(imagesPath, option)
            .then((img) => {
            img.write(output, () => { callback(null, output); });
        })
            .catch(e => { console.log(e); callback(e, null); });
    }
    static getImage64(path, callback) {
        base64Img.base64(path, (e, d) => {
            callback(e, d);
        });
    }
    //gets the creationtime by exif, fallback is file birthtime
    static creationTime(filePath, callback) {
        const i = new Image(filePath);
        Image.exif(filePath, (err, data) => {
            if (err) { //usually TypeError: Cannot read property 'exif' of null
                console.log('IMAGE EXIF ERROR ', err);
            }
            //fallback to other methods even with error
            let fallbackfsstats = true;
            if (data) {
                if (data.exif) {
                    if (data.exif.DateTimeOriginal) {
                        callback(null, data.exif.DateTimeOriginal);
                        fallbackfsstats = false;
                    }
                }
            }
            if (fallbackfsstats) {
                fs.stat(filePath, function (error, stats) {
                    if (error) {
                        callback(error, null);
                        console.log('fs.stat birtime error', error);
                        return;
                    }
                    callback(null, stats.birthtime);
                });
            }
        });
    }
    //returns original, compressed, and resized compressed in Image64 from Image64. 
    //fileOut = false will delete all three files, use it if you just want the three image64String
    //WARNING this method is known to do callback multiple times due to getImage64 > base64Img.base64(). It is a bug.
    static Image64toCompress(image64String, maxWidth, callback, fileOut = false) {
        const tempName = GUID_1.GUID.create();
        const tempNameOriginal = tempName + '_o';
        const tempNameCompress = tempName + '_c';
        const tempNameCompressResized = tempName + '_cr';
        let imageCompress;
        let imageSmall;
        let ext;
        this.writeImage64(image64String, './', tempNameOriginal, (err, originalFile) => {
            const im = new Image('./' + originalFile);
            ext = originalFile.substring(originalFile.length - 4, originalFile.length);
            const get64Compress = (callback1) => {
                im.Compress(tempNameCompress + ext, (e, r) => {
                    Image.getImage64(r, (e, d) => callback1(e, d));
                });
            };
            const get64CompressAndResize = (callback2) => {
                im.CompressAndResize(tempNameCompressResized + ext, maxWidth, (e, r) => {
                    Image.getImage64(r, (e, d) => callback2(e, d));
                });
            };
            async.series([get64Compress, get64CompressAndResize], (err, result) => {
                if (!fileOut) {
                    console.log('deleting');
                    fs.unlinkSync(tempNameCompress + ext);
                    fs.unlinkSync(tempNameCompressResized + ext);
                    fs.unlinkSync(originalFile);
                }
                callback(image64String, result[0], result[1], err);
                // File.delIfExist(tempNameCompress + ext, d=>console.log(d));
                // File.delIfExist(tempNameCompressResized + ext, d=>console.log(d));
                // File.delIfExist(originalFile, d=>console.log(d));       
            });
        });
    }
    static writeImage64(image64String, folderPath, fileName, callback) {
        base64Img.img(image64String, folderPath, fileName, (e, d) => {
            callback(e, d); //d is the image name ex: image.jpg
        });
    }
    //this is only async. It is assume that all nNames frames are the same dimension
    //path should be name of the folder without / in the end "../frames"
    //nNames should be like "frame?.png" where ? is 1,2,3,4...
    //output can be "../folder/myGif.gif"
    //repeat: 0 means infinity
    static pngToGif(path, nNames, output, callback) {
        const pathFix = path + '/**/' + nNames;
        fs.readdir(path, (e, d) => {
            console.log(d);
            if (e) {
                throw e;
            }
            if (d.length < 1) {
                if (callback) {
                    console.log('no screens to build from');
                    callback(true, { error: 'no screens to build from' });
                }
            }
            const statsPath = path + '\\' + d[1];
            stat.stats(statsPath, dim => {
                const end = () => {
                    if (callback) {
                        console.log('gif created ', output);
                        callback(null, { success: 'gif was built' });
                    }
                };
                var encoder = new gifEncoder(dim.width, dim.height);
                pngStream(pathFix)
                    .pipe(encoder.createWriteStream({ repeat: 0, delay: 100, quality: 30 }))
                    .pipe(fs.createWriteStream(output))
                    .on('finish', end);
            });
        });
    }
    //gets the brightness of a square region, origin should be the topleft, end should be the lowerleft
    //1 is full bright, 0 is full black
    static shadePortion(filePath, origin, end, callback) {
        Image.stats(filePath, (err, dim) => {
            if (err) {
                return;
            }
            console.log('Image stats ', dim);
            Image.pixelSquare(origin.x, origin.y, end.x, end.y, dim.width, dim.height, (err, n) => {
                console.log('n pixels length ', n.length);
                Image.shade(filePath, n, (err, shade) => {
                    if (err) {
                        console.log('ERROR shadePortion ', err);
                        callback(err, null);
                        return;
                    }
                    callback(null, shade);
                });
            });
        });
    }
    //if n is null, all pixels are used
    //if shade is 1, photo should be all white
    static shade(filePath, n, callback) {
        //returns all pixels in on long array of {r,g,b} of 255 as pixels[0]
        //255 for all means white
        pixelGetter.get(filePath, (err, pixels) => {
            if (err) {
                console.log('ERROR shade ', err);
                callback(err, null);
                return;
            }
            const joinValues = [];
            let shade;
            if (!n) {
                _.forEach(pixels[0], (e) => {
                    joinValues.push(e.r, e.g, e.b);
                });
                shade = _.mean(joinValues) / 255;
            }
            else {
                _.forEach(n, e => {
                    const location = [pixels[0][e.n].r, pixels[0][e.n].g, pixels[0][e.n].b];
                    joinValues.push(_.mean(location));
                    // console.log(_.mean([pixels[0][e.n].r, pixels[0][e.n].g, pixels[0][e.n].b]))
                });
                shade = _.mean(joinValues) / 255;
            }
            callback(null, shade);
        });
    }
    //returns square pixel index that can be use with pixelGetter. starts at upperleft for x0,y0 and ends at lowerRight for x1,y1
    static pixelSquare(x0, y0, x1, y1, horizontal, vertical, callback) {
        if (y0 > y1 || x0 > x1) {
            callback('ERROR start greater than end values', null);
            return;
        }
        if (x0 > horizontal) {
            x0 = horizontal;
        }
        if (y0 > vertical) {
            y0 = vertical;
        }
        if (x1 > horizontal) {
            x1 = horizontal;
        }
        if (y1 > vertical) {
            y1 = vertical;
        }
        const resolution = horizontal * vertical;
        const n = (x, y) => {
            const n = (horizontal) * y + x;
            return n;
        };
        const list = [];
        for (let i = y0; i < y1; i++) { //vertical
            for (let j = x0; j < x1; j++) { //horizontal
                list.push({ n: n(j, i), x: j, y: i });
                // console.log({n: n(j, i), x: j, y: i})
            }
        }
        callback(null, list);
    }
}
exports.Image = Image;
//# sourceMappingURL=Image.js.map