"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cjs = require("crypto-js");
class Cipher {
    constructor(key) {
        this.key = key;
    }
    Encrypt(inputText) {
        return cjs.AES.encrypt(inputText, this.key).toString();
    }
    Decrypt(inputEncryptedText) {
        return cjs.AES.decrypt(inputEncryptedText, this.key).toString(cjs.enc.Utf8);
    }
    static Hash(inputText) {
        return cjs.SHA256(inputText).toString();
    }
}
exports.Cipher = Cipher;
//# sourceMappingURL=Cipher.js.map