"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("request");
const stocksModel_1 = require("../../Stocks/StocksNode/Model/stocksModel");
const events_1 = require("events");
class AlphaVantage {
    constructor() {
        this.emitter = new events_1.EventEmitter();
    }
    dailySeries(symbol, AVkey, isFull = false) {
        let size;
        if (isFull) {
            size = 'full';
        }
        else {
            size = 'compact';
        }
        const URLCall = `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${symbol}&outputsize=${size}&apikey=${AVkey}`;
        console.log({ uri: URLCall, timeout: 15000 });
        request(URLCall, (err, res, body) => {
            if (err) {
                this.emitter.emit('completed', false);
                return;
            }
            const b = JSON.parse(body);
            if (b['Error Message']) {
                this.emitter.emit('completed', false);
                return;
            }
            const data = b['Time Series (Daily)'];
            const keys = Object.keys(data);
            const stockList = [];
            keys.forEach(k => {
                const model = new stocksModel_1.StocksModel();
                model.dayId = k;
                model.symbol = symbol;
                model.vOpen = parseInt(data[k]['1. open']);
                model.vHigh = parseInt(data[k]['2. high']);
                model.vLow = parseInt(data[k]['3. low']);
                model.vClose = parseInt(data[k]['4. close']);
                model.volume = parseInt(data[k]['5. volume']);
                model.time = new Date(k);
                stockList.push(model);
            });
            this.emitter.emit('completed', stockList);
        });
    }
}
exports.AlphaVantage = AlphaVantage;
//# sourceMappingURL=AlphaVantage.js.map