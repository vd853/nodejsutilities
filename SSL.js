"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const File_1 = require("./File");
const fs = require("fs");
class SSL {
    //openSSLBinPath is only use for windows
    //for linux, install it, and test from ssh that "openssl" will call openssl
    static validatePEM(openSSLBinPath, pemFile, keyPassphrase, isKey, callback) {
        let path = File_1.File.reparsePath(openSSLBinPath) + '/openssl';
        path = process.platform === "win32" ? path : 'openssl';
        if (process.platform === "win32") {
            if (!fs.existsSync(path + '.exe')) {
                callback(('openssl not found: ' + path + '.exe'), null);
                return;
            }
        }
        const file = File_1.File.reparsePath(pemFile);
        if (!fs.existsSync(file)) {
            callback('pem file not found', null);
            return;
        }
        if (isKey) {
            child_process_1.exec(path + ' rsa -inform PEM -in ' + file + ' -passin pass:' + keyPassphrase, (e, s) => {
                if (e) {
                    callback(e, null);
                    return;
                }
                const reg1 = new RegExp('-----END RSA PRIVATE KEY-----');
                if (!reg1.test(s)) {
                    callback(null, false);
                    return;
                }
                callback(null, true);
            });
        }
        else {
            child_process_1.exec(path + ' x509 -inform PEM -in ' + file + ' -passin pass:' + keyPassphrase, (e, s) => {
                if (e) {
                    callback(e, null);
                    return;
                }
                const reg2 = new RegExp('-----END CERTIFICATE-----');
                if (!reg2.test(s)) {
                    callback(null, false);
                    return;
                }
                callback(null, true);
            });
        }
    }
}
exports.SSL = SSL;
//# sourceMappingURL=SSL.js.map