"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const shelljs = require("shelljs");
const glob = require("glob");
const crypto = require("crypto");
const fse = require("fs-extra");
const del2 = require("delete");
const zip = require("jszip");
const async = require("async");
const mv = require("mv");
class File {
    static getSubfolders(folderPath) {
        return fs.readdirSync(folderPath).filter(f => fs.statSync(path.join(folderPath, f)).isDirectory());
    }
    static getAllFiles(folderPath) {
        return fs.readdirSync(folderPath).filter(f => fs.statSync(path.join(folderPath, f)).isFile());
    }
    static getFolder(filePath) {
        return path.dirname(filePath);
    }
    static getPathOnly(filePath) {
        return this.reparsePath(filePath).replace(path.basename(filePath), '');
    }
    //given a file name, not path, returns that file name without its extension
    //input: note.txt, output: note || input: not.e.txt, output: note.e || input: note, output: note
    //if you want file name with extension from a full path, use getFileOnlyOrParentFolderName()
    static getFileNameOnly(fileName) {
        const splitter = fileName.split('.');
        console.log(splitter);
        if (splitter.length < 2) {
            return splitter[0];
        }
        let joiner = '';
        for (let i = 0; i < splitter.length - 1; i++) {
            joiner += splitter[i];
            if (i < splitter.length - 2) {
                joiner += '.';
            }
        }
        return joiner;
    }
    static reparsePath(filePath) {
        return path.resolve(filePath);
    }
    static move(pathOriginal, pathNew, callback) {
        fse.move(pathOriginal, pathNew, err => {
            if (callback) {
                if (err) {
                    callback(err);
                }
                else {
                    callback(null);
                }
            }
        });
    }
    static move2(pathOriginal, pathNew, callback) {
        mv(pathOriginal, pathNew, err => {
            if (callback) {
                if (err) {
                    callback(err);
                }
                else {
                    callback(null);
                }
            }
        });
    }
    //does not include the dot
    static getExtension(fileName) {
        return fileName.substr(fileName.length - 3, 3);
    }
    static deleteFolder(filePath, callback) {
        //method3
        del2(filePath, (err, deleted) => {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, deleted);
            }
        });
        //method2
        // del(filePath)
        // .then(e=>callback(e))
        //method1
        // fse.remove(filePath, err=>{
        //     if(err){
        //         console.log('deleteFolder error: ', err)
        //         callback(err)
        //         return
        //     }
        //     callback(true)
        // })
        //Method0
        // var deleteFolderRecursive = function(path) {
        //     if( fs.existsSync(path) ) {
        //         fs.readdirSync(path).forEach(function(file) {
        //             var curPath = path + "/" + file;
        //             if(fs.statSync(curPath).isDirectory()) { // recurse
        //                 deleteFolderRecursive(curPath);
        //             } else { // delete file
        //                 fs.unlinkSync(curPath);
        //             }
        //         });
        //         fs.rmdirSync(path);
        //         }
        // };
        // deleteFolderRecursive(filePath)
        // callback({result: true})
    }
    static getFileOnlyOrParentFolderName(filePath) {
        return path.basename(filePath);
    }
    static delIfExist(filepath, callback) {
        fs.exists(filepath, d => {
            if (d) {
                console.log('file deleting ', filepath);
                fs.unlink(filepath, (e) => {
                    if (e) {
                        callback(e);
                        return;
                    }
                    callback(null);
                });
                return;
            }
            callback(filepath + ' does not exist');
        });
    }
    //this can make folders recursively
    static createFolder(folderPath) {
        if (!fs.existsSync(folderPath)) {
            shelljs.mkdir('-p', folderPath);
            return true;
        }
        else {
            return false;
        }
    }
    static CopyFileIfNotExist(inputPath, outputPath, callback) {
        const condition = fs.existsSync(outputPath);
        if (!condition) {
            this.CopyFile(inputPath, outputPath, callback);
        }
        else {
            callback('file not exist', null);
        }
    }
    static CopyFiles(inputPath, outputPath, callback) {
        const tasks = [];
        console.log('copy files source ', inputPath);
        console.log('copy files targets ', outputPath);
        outputPath.forEach(p => {
            const task = (callback) => {
                fs.createReadStream(inputPath)
                    .pipe(fs.createWriteStream(p)
                    .on('error', err => { console.log(err); callback(err, null); return; }))
                    .on('error', err => { console.log(err); callback(err, null); return; })
                    .on('finish', data => {
                    console.log('file copied ', p);
                    callback(null, data);
                });
            };
            tasks.push(task);
        });
        async.parallel(tasks, callback);
    }
    static CopyFile(inputPath, outputPath, AfterCopy) {
        let wStream;
        const stream = fs.createReadStream(inputPath)
            .pipe(wStream = fs.createWriteStream(outputPath)
            .on('error', err => { AfterCopy(err, null); return; }))
            .on('error', err => { AfterCopy(err, null); return; })
            .on('finish', data => {
            AfterCopy(null, data);
            //{result: data, streams:[wStream, stream]}
            // console.log('stream destory rw')
            // stream.destroy();
            // wStream.destroy();
        });
    }
    static GetObjectFromJSONFile(file, callback) {
        fs.readFile(file, 'utf8', (error, data) => {
            callback(JSON.parse(data));
        });
    }
    static SetJSONFileFromObject(file, obj, callback) {
        fs.writeFile(file, JSON.stringify(obj), 'utf8', callback);
    }
    //checks if A is newer than B
    static isNewer(fileA, fileB, callback) {
        const conditionA = fs.existsSync(fileA);
        const conditionB = fs.existsSync(fileB);
        if (!conditionA || !conditionB) {
            callback(true);
            return;
        }
        fs.stat(fileA, (e, statA) => {
            fs.stat(fileB, (e, statB) => {
                console.log('modifydateA: ' + statA.mtime + ' modifydateB: ' + statB.mtime);
                if (statA.mtime > statB.mtime) {
                    callback(true);
                }
                else {
                    callback(false);
                }
            });
        });
    }
    static getCheckSum(path, callback) {
        if (!fs.existsSync(path)) {
            console.log('getCheckSum FILE NOT EXIST!');
            return;
        }
        ;
        const hash = crypto.createHash('md5');
        const stream = fs.createReadStream(path);
        stream.on('data', d => {
            hash.update(d, 'utf8');
        });
        stream.on('end', function () {
            callback(hash.digest('hex'));
        });
    }
    static getAllFilesRecursive(path, callback) {
        const finder = path + '/**/*';
        glob(finder, function (err, data) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, data);
            }
        });
    }
    //zips all files in the folderPath. The root folderPath folder will be create in the zip
    //all empty folders will be skipped
    //if cleanup is set true, the folderPath will be removed after the zip is created
    //data will be true if successful, otherwize, data will be null with some err
    static zipFolder(folderPath, output, callback, cleanUp = false) {
        //get all files and folder in folderPath
        File.getAllFilesRecursive(folderPath, (err, data) => {
            if (err) {
                console.log('ERROR getallfilesrecurseive ', err);
                callback(err, null);
                return;
            }
            //get the name of the folderPath
            const mainFolderName = File.getFileOnlyOrParentFolderName(folderPath);
            if (data.length < 1) {
                console.log('ERROR getallfilesrecursive, no files to zip');
                callback('no files to zip', null);
                return;
            }
            //create a jszip instance
            const zipper = new zip();
            //main task to process async parallel
            const processZip = (callback) => {
                const tasks = [];
                //foreach file, read it and add it to the zip instance
                data.forEach(file => {
                    const eachFile = (callback1) => {
                        const target = file.replace(folderPath, mainFolderName);
                        const isFolder = fs.lstatSync(file).isDirectory();
                        console.log('zip file source ', target, ' is directory ', isFolder);
                        //adds files only
                        if (!isFolder) {
                            //read file as binary and add to zip
                            fs.readFile(file, 'binary', (err, data1) => {
                                if (err) {
                                    console.log('ERROR getallfilesrecurseive readFile', err);
                                    callback1(err, null);
                                    return;
                                }
                                zipper.file(target, data1, { binary: true });
                                console.log('zip file added ', file);
                                callback1(null, true);
                            });
                        }
                        else {
                            callback1(null, true);
                        }
                    };
                    //eachFile is a task to be process
                    tasks.push(eachFile);
                });
                //process all the tasks
                async.parallel(tasks, (err, data) => {
                    if (err) {
                        console.log('ERROR getallfilesrecurseive async tasks', err);
                        callback(err, null);
                        return;
                    }
                    //callback to the processZip
                    callback(null, true);
                });
            };
            processZip((err, data) => {
                if (err) {
                    console.log('ERROR getallfilesrecurseive processZip', err);
                    callback(err, null);
                    return;
                }
                console.log('Creating zip');
                //creates the actual zip files once all folders and files are added
                zipper
                    .generateNodeStream({ type: 'nodebuffer', streamFiles: true })
                    .pipe(fs.createWriteStream(output))
                    .on('error', e => {
                    callback(e, null);
                    return;
                })
                    .on('finish', function () {
                    console.log(output + ' zip file created');
                    if (cleanUp) {
                        File.deleteFolder(folderPath, (err, data) => {
                            if (err) {
                                console.log('ERROR getallfilesrecurseive cleanUp', err);
                                callback(err, null);
                                return;
                            }
                            console.log('Clean up was used, deleted: ', folderPath);
                            callback(null, true);
                        });
                    }
                    else {
                        callback(null, true);
                    }
                });
            });
        });
    }
    //takes a list of files and put them in the a zip file, will not put them in any folder or subfolder
    static zipFiles(filePaths, output, callback) {
        if (filePaths.length < 1) {
            console.log('no files to zip');
            callback('no files to zip', false);
            return;
        }
        //create a jszip instance
        const zipper = new zip();
        //main task to process async parallel
        const processZip = (callback) => {
            const tasks = [];
            //foreach file, read it and add it to the zip instance
            filePaths.forEach(file => {
                const eachFile = (callback1) => {
                    //check if this file exist
                    const isExist = fs.existsSync(file);
                    if (!isExist) {
                        console.log('File does not exist for zipping, skip ', file);
                        callback1(null, true);
                        return;
                    }
                    const singleFile = File.getFileOnlyOrParentFolderName(file); //gets file name only with extension
                    const isFolder = fs.lstatSync(file).isDirectory(); //check if it is not a folder
                    //adds files only
                    if (!isFolder) {
                        //read file as binary and add to zip
                        fs.readFile(file, 'binary', (err, data1) => {
                            if (err) {
                                console.log('ERROR zipFiles readFile', err);
                                callback1(err, null);
                                return;
                            }
                            zipper.file(singleFile, data1, { binary: true });
                            console.log('zip file added ', singleFile);
                            callback1(null, true);
                        });
                    }
                    else {
                        callback1(null, true);
                    }
                };
                //eachFile is a task to be process
                tasks.push(eachFile);
            });
            //process all the tasks
            async.parallel(tasks, (err, data) => {
                if (err) {
                    console.log('ERROR zipFiles async tasks', err);
                    callback(err, null);
                    return;
                }
                //callback to the processZip
                callback(null, true);
            });
        };
        processZip((err, data) => {
            if (err) {
                console.log('ERROR zipFiles processZip', err);
                callback(err, null);
                return;
            }
            console.log('Creating zip');
            //creates the actual zip files once all folders and files are added
            zipper
                .generateNodeStream({ type: 'nodebuffer', streamFiles: true })
                .pipe(fs.createWriteStream(output))
                .on('error', e => {
                callback(e, null);
                return;
            })
                .on('finish', function () {
                console.log(output + ' zip file created');
                callback(null, true);
            });
        });
    }
    //takes a file and modifies a value inside that file
    //ex. NETMASK=123.123 that is inside a file can be change to NETMASK=456.456
    static ModifyConfig(filePath, field, value, isGet, callback) {
        if (!fs.existsSync(filePath)) {
            callback('Modify config file not exist.', null);
            return;
        }
        fs.readFile(filePath, 'utf8', function (err, data) {
            if (err) {
                callback(err, null);
                return;
            }
            const r = new RegExp(field + '=.*', 'g');
            const match = data.match(r);
            if (match === null)
                throw 'Invalid network-script definition, no = was match with field';
            if (match.length > 0) {
                if (isGet) {
                    callback(null, match[0].replace(field + '=', ''));
                    return;
                }
                data = data.replace(match[0], field + '=' + value);
                fs.writeFile(filePath, data, function (err) {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    callback(null, true);
                });
            }
            else {
                callback('Field doesnt exist', null);
            }
        });
    }
}
exports.File = File;
class Transfer {
    static assembileBuffers(slicedSegments, callback) {
        callback(Buffer.concat(slicedSegments));
    }
}
exports.Transfer = Transfer;
//Use to watch file changes then copies
class Watch {
    constructor(sourcePath, //the exact name of the folder in your project to watch over. Watches files recersively
    destinationPath, ext = '.js', callback) {
        this.sourcePath = sourcePath;
        this.destinationPath = destinationPath;
        this.ext = ext;
        this.callback = callback;
        this.fileWatching = [];
        this.EventRaisedTime = 0;
        this.hold = false;
        this.disable = false; //disables entire script if target is newer
        this.data = new Map();
        this.dataFile = 'Watcher.JSON';
        this.dataFileFullPath = '';
        const finder = sourcePath + '/**/*' + ext;
        console.log('Finder', finder);
        this.data['test'] = 'works';
        var start = () => this.Start();
        var define = e => { this.fileWatching = e; };
        var setDataFile = () => {
            this.SetDataFile(true, () => {
                callback(); //use in factory to start first copying
                start(); //start watching
            });
        };
        glob(finder, function (err, res) {
            if (err) {
                console.log('Error', err);
            }
            else {
                res.forEach(element => {
                    console.log('Watching: ' + element);
                });
                define(res); //defines files to be watch
                setDataFile();
            }
        });
    }
    SetDataFile(isGet, callback) {
        if (isGet) {
            this.dataFileFullPath = this.destinationPath + '/' + this.dataFile;
            if (!fs.existsSync(this.dataFileFullPath)) {
                console.log('Setting up new data file');
                File.SetJSONFileFromObject(this.dataFileFullPath, this.data, callback);
            }
            else {
                console.log('Getting from previous data file');
                File.GetObjectFromJSONFile(this.dataFileFullPath, (d) => {
                    this.data = new Map();
                    for (let id in d) {
                        this.data[id] = d[id];
                    }
                    if (callback)
                        callback();
                });
            }
        }
        else {
            File.SetJSONFileFromObject(this.dataFileFullPath, this.data, callback);
        }
    }
    static Factory(sourcePath, //the exact name of the folder in your project to watch over. Watches files recersively
    destinationPath, ext = '.js') {
        if (!fs.existsSync(sourcePath) || !fs.existsSync(destinationPath)) {
            console.log('A path does not exist, will exit');
            process.exit();
        }
        const w = new Watch(sourcePath, destinationPath, ext, () => {
            w.fileWatching.forEach(e => {
                w.Copier(e);
                console.log('Started first file copy');
            });
        });
        return w;
    }
    Start() {
        this.fileWatching.forEach(f => {
            fs.watch(f, (e, t) => {
                if (!this.hold) {
                    this.EventRaisedTime++;
                    if (!this.disable) {
                        const c = this.CopyConstructor(t);
                        console.log(this.EventRaisedTime + ' Event: ' + e + ' on file ' + c.thisFile);
                        console.log('Copy to ' + c.outFile);
                    }
                    else {
                        console.log('Watcher is disabled. Your target is NEWER. You must delete the target and this JSON data file and restart this script.');
                    }
                    this.hold = true;
                }
                //delay due to bug
                setTimeout(() => {
                    this.hold = false;
                }, 100);
            });
        });
    }
    //creates full path base on source and pass to copier
    CopyConstructor(file) {
        const originalFullPath = this.sourcePath + '/' + file;
        console.log('originalfullpath', originalFullPath);
        return this.Copier(originalFullPath);
    }
    //use source path to create target path and starts copying
    Copier(fileOriginalPath) {
        const file = File.getFileOnlyOrParentFolderName(fileOriginalPath);
        const outfile = this.destinationPath + '/' + file;
        if (this.data[outfile]) {
            File.getCheckSum(outfile, d => {
                if (this.data[outfile] === d) {
                    File.CopyFile(fileOriginalPath, outfile, () => {
                        File.getCheckSum(outfile, (sum) => {
                            console.log('Updating file');
                            this.data[outfile] = sum; //set sum to data
                            //console.log('setting data and file', this.data);
                            this.SetDataFile(false, () => { }); //store data
                        });
                    });
                }
                else {
                    this.disable = true;
                    console.log('NOT COPY: checksum did not match old data, so it was changed outside this script');
                }
            });
        }
        else {
            console.log('setting new entry in data');
            console.log(fileOriginalPath);
            console.log(outfile);
            //should be the same as above for copy condition
            File.CopyFile(fileOriginalPath, outfile, () => {
                console.log('New file entry in data');
                File.getCheckSum(outfile, (sum) => {
                    this.data[outfile] = sum; //set sum to data
                    //console.log('setting data and file', this.data);
                    this.SetDataFile(false, () => { }); //store data
                });
            });
        }
        return { thisFile: fileOriginalPath, outFile: outfile };
    }
}
exports.Watch = Watch;
//# sourceMappingURL=File.js.map