import * as winston from 'winston';
import {File} from './File';
export class Logger{
    private logg: any;
    constructor(title: string, path: string, maxSize = 10000, maxFiles = 10){
        File.createFolder(path)
        var myCustomLevels = {
            levels: {
                info: 0,
                warn: 1,
                error: 2,
            },
            colors: {
                info: 'green',
                warn: 'yellow',
                error: 'red'
            }
        };
        const transports = {
            levels: myCustomLevels.levels,
            console: new winston.transports.Console(),
            fileInfo: new winston.transports.File({ 
                filename: path+'/logInfo.log', 
                level: 'info', 
                maxsize: maxSize,
                maxFiles: maxFiles,
                tailable: true,
            }),
            fileWarn: new winston.transports.File({ 
                filename: path+'/logWarn.log', 
                level: 'warn', 
                maxsize: maxSize,
                maxFiles: maxFiles,
                tailable: true,
            }),
            fileError: new winston.transports.File({ 
                filename: path+'/logError.log', 
                level: 'error', 
                maxsize: maxSize,
                maxFiles: maxFiles,
                tailable: true,
            })
        }
        this.logg = winston.createLogger({
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.label({ label: title }),
                winston.format.colorize(),
                winston.format.simple(),
                ),
            transports: [
                transports.console,
                transports.fileInfo,
                transports.fileError,
                transports.fileWarn
            ]
        });
        winston.addColors(myCustomLevels.colors);
    }

    //logging is prioritized, so info will get all levels, while warn will only get warn and error. This is only true for files.
    log(message: string, label?: string){
        this.logg.info({
            message: message + (label? ' : ' + label: ''),
        })
    }
    error(message: string, label?: string){
        this.logg.error({
            message: message + (label? ' : ' + label: ''),
        })
    }
    warn(message: string, label?: string){
        this.logg.warn({
            message: message + (label? ' : ' + label: ''),
        })
    }
}