import * as mongoose from 'mongoose'
import * as _ from 'lodash';
import { isNullOrUndefined } from 'util';
class Connector{
    mongoDB = 'mongodb://127.0.0.1/maindb';
    opt = {
        "auth": { "authSource": "admin" },
        "user": "admin",
        "pass": "admin"
    }
    public constructor(private schema: any, task:(error, result)=>void){
        mongoose.connect(this.mongoDB, this.opt);
        mongoose.connection.on('error', ()=>{
            console.error.bind(console, 'MongoDB connection error:')
            task(true, null)
            return;
        });
        mongoose.connection.once('open', function() {
                // console.log('mongoose connected!')
                task(null, true);
            }
        )
    }
}
export class Mongo{
    actionModel: any; //this will be use to preform all the operations
    constructor(private name: string, private schema: any, private index: any){
        if(index){
            console.log('index use')
            this.actionModel = mongoose.model(name, new mongoose.Schema(schema).index(index))
        }else{
            console.log('no index use')
            this.actionModel = mongoose.model(name, new mongoose.Schema(schema))
        }
    }

    //if successful, will return exactly the new model
    AddOrUpdate(search, data, callback:(error, result)=>void){
        this.FindOne(search, (e,r)=>{
            if(e){
                console.log('find one error ', e)
                callback(e,null)
                return;
            }
            if(!r){
                data.modifiedDate = new Date()
                data.createdDate = new Date()
                const c = new Connector(this.schema, (error0, result0)=>{
                    this.actionModel.create(data, (error1, result1)=>{
                        if(error1) {
                            console.log('error create ', error1)
                            console.log('error model ', data)
                        }
                        console.log('new entry added ', true)
                        if(callback){
                            callback(error1, data)
                        }
                    })
                })
            }else{
                data.modifiedDate = new Date()
                const c = new Connector(this.schema, (error0, result0)=>{
                    this.actionModel.update(search, data, (error2, result2)=>{
                        if(error2) console.log('error update ', error2, ' model ', data)
                        console.log('updated entry ', result2)
                        if(callback){
                            callback(error2, data)
                        }
                    })
                })
            }
        })
    }

    //removes or add an item to a field array.
    Array(search: any, field: string, value: string, isAdd: boolean, callback:(err, result)=>void){
        const c = new Connector(this.schema, (err, result)=>{
            if(err){
                callback(err, null)
                return;
            }
            const location = {}
            location[field] = value
            if(isAdd){
                this.actionModel
                .update(search, {$push: location}, (err, result)=>{
                    if(err){
                        callback(err, null)
                        console.log('error in FindInArray')
                        return;
                    }
                    callback(err, result);
                })
            }else{
                this.actionModel
                .update(search, {$pull: location}, (err, result)=>{
                    if(err){
                        callback(err, null)
                        console.log('error in FindInArray')
                        return;
                    }
                    callback(err, result);
                })
            }
        }) 
    }

    GetCount(search, callback:(error, result)=>void){
        const c = new Connector(this.schema, (error0, result0)=>{
            this.actionModel.count(search, (error, result)=>{
                if(error){
                    callback(error, null)
                    console.log('error in count ', error)
                    return;
                }
                callback(null, result)
            })
        })
    }

    //will return result as null if nothing is found
    FindOne(search, callback:(error, result)=>void){
        const c = new Connector(this.schema, (error0, result0)=>{
            this.actionModel.findOne(search, (error, result)=>{
                if(error){
                    callback(error, null)
                    console.log('error in findone ', error)
                    return;
                }
                if(result){
                    console.log('FindOne: ', true)
                }else{
                    console.log('FindOne: ', false)
                }
                callback(null, result)
            })
        })
    }

    //arg example: 'attr', 'dog0', 'hat0' where dog0 is an object inside attr and hat0 is a value of dog0
    FindOneAttr(attrName: string, attrField: string, attrValue: string, callback:(err, result)=>void){
        const attrFinderObj = {}
        attrFinderObj[attrName + '.' + attrField] = attrValue; 
        this.FindOne(attrFinderObj, callback);
    }

    FindAll(search, callback:(error, result)=>void){
        const c = new Connector(this.schema, (error0, result0)=>{
            this.actionModel.find(search, (error, result)=>{
                if(error){
                    callback(error, null)
                    console.log('error in FindAll')
                    return;
                }
                //console.log('FindAll: ', result.length)
                callback(null, result)
            })
        })
    }

    FindInArray(field: string, arrayValues: string[], callback:(err, result)=>void){
        const c = new Connector(this.schema, (err, result)=>{
            if(err){
                callback(err, null)
                return;
            }
            const search = {}
            search[field] = {$all: arrayValues}
            this.actionModel
            .find(search, (err, result)=>{
                if(err){
                    callback(err, null)
                    console.log('error in FindInArray')
                    return;
                }
                callback(err, result);
            })
        }) 
    }

    //creates a connection and returns action model for custom queues
    FindCustom(callback:(error, actionModel)=>void){
        const c = new Connector(this.schema, (error, result)=>{
            if(error){
                callback(error, null)
                return;
            }
            callback(null, this.actionModel)
        }) 
    }

    //For any search, it will return all the result in the distinctField with no duplicates
    FindDistinct(search: any, distinctField: string, callback:(error, result)=>void){
        const c = new Connector(this.schema, (err, result)=>{
            if(err){
                callback(err, null)
                return;
            }
            this.actionModel
            .find(search)
            .distinct(distinctField, (err, result)=>{
                if(err){
                    callback(err, null)
                    return;
                }
                callback(null, result)
            })
        }) 
    }

    //requires sorting field and return returnFields
    FindText(search: string, sort:Array<{field: string, decending: boolean}>,returnFields: string[], callback:(err, result)=>void){
        let sorts: [string, number][];
        sort.forEach(e=>{
            if(!sorts){
                sorts = [[e.field, e.decending? -1: 1]]
            }else{
                sorts.push([e.field, e.decending? -1: 1])
            }
        })
        const c = new Connector(this.schema, (error0, result0)=>{
            this.actionModel
            .find({$text: {$search: search}})
            .select(returnFields)
            .sort(sorts)
            .exec((error, result)=>{
                if(error){
                    callback(error, null)
                    console.log('error in FindText')
                    return;
                }
                callback(null, result)
            })
        })
    }

    //same as FindText except result MUST contain all the terms
    FindTextAnd(search: string, sort:Array<{field: string, decending: boolean}>,returnFields: string[], callback:(err, result)=>void){

        //splits search string terms into array, add quotations around each term, then combines back into string
        const fixSearch = _.map(search.split(' '), e=> {return '"' + e + '"'}).join(' ');
        this.FindText(fixSearch, sort, returnFields, callback);
    }

    Remove(search, callback:(error, result)=>void){
        const c = new Connector(this.schema, (error0, result0)=>{
            this.actionModel.remove(search, (error, result)=>{
                if(error){
                    callback(error, null)
                    console.log('error in remove')
                    return;
                }
                console.log('Remove: ', result)
                callback(null, result)
            })
        })
    }
    RemoveAll(callback?:(error, result)=>void){
        this.Remove({}, (e,r)=>{
            if(callback){
                callback(e, r)
            }
        })
    }

    //For any matches in field, return the result in only some fields
    //Example, you have a list of _ids and want to get back their first and last names only
    FindAllForFields(field: string, matches: string[], returnFields: string[], callback:(err, result)=>void){
        //search is use this way to define an object key with a variable, in this case the variable is field
        const search: any = [];
        search.push({[field]: {$in: matches}}) 
        const c = new Connector(this.schema, (error0, result0)=>{
            this.actionModel.find(search[0], returnFields, (error, result)=>{
                if(error){
                    callback(error, null)
                    console.log('error in FindAllForFields')
                    return;
                }
                console.log('FindAllForFields: ', result.length)
                callback(null, result)
            })
        })
    }

    //returns result only in the selected fields instead of all fields
    FindAllReturnFields(search, fields: string[], callback:(error, result)=>void){
        const c = new Connector(this.schema, (error0, result0)=>{
            this.actionModel.find(search, fields, (error, result)=>{
                if(error){
                    callback(error, null)
                    console.log('error in FindAll')
                    return;
                }
                console.log('FindAllReturnFields: ', result.length)
                callback(null, result)
            })
        })
    }

    //use for ascending and descending sorting, will return everything unless limited by search param
    //3rd params returnFields is use to limit the return field, example, you only want the id fields. Use [] for all fields
    //Example of using or, results will return if entry match photographer or comment
    // const searcher = {
    //     $and: [
    //         { $or: [{photographer: 'vd'}, {comment: 'vd'}] }
    //     ]
    // }
    FindWhereUnlimitedSort(search, sort:Array<{field: string, decending: boolean}>, returnFields: string[], callback:(error, result)=>void){
        //all sorts will use dec
        let sorts: [string, number][];
        sort.forEach(e=>{
            if(!sorts){
                sorts = [[e.field, e.decending? -1: 1]]
            }else{
                sorts.push([e.field, e.decending? -1: 1])
            }
        })
        const c = new Connector(this.schema, (error0, result0)=>{
            if(error0){
                console.log('error in FindWhereUnlimited ', error0)
                callback(error0, null)
                return;
            }            
            this.actionModel
            .find()
            .where(search)
            .select(returnFields)
            .sort(sorts)
            .exec((error, result)=>{
                if(error){
                    console.log('error in FindWhereUnlimited ', error)
                    callback(error, null)
                    return;
                }
                console.log('FindWhereUnlimited: ', result.length)
                callback(null, result)
            })
        })
    }

    //limit by paginate, order, lt, gt
    //Only lt and gt can be null
    FindWhere(search, start = 0, amount = 10, lt:any, gt:any, sort = '_id', callback:(error, result)=>void){
        const c = new Connector(this.schema, (error0, result0)=>{
            if(error0){
                callback(error0, null)
                console.log('error in FindWhereUnlimited ', error0)
                return;
            }
            if(!gt && !lt){
                this.actionModel
                .find()
                .where(search)
                .skip(start)
                .limit(amount)
                .sort(sort)
                .exec((error, result)=>{
                    if(error){
                        callback(error, null)
                        console.log('error in FindWhere ', error)
                        return;
                    }
                    console.log('FindWhere: ', result)
                    callback(null, result)
                })
                return;
            }
            this.actionModel
            .find()
            .where(search)
            .skip(start)
            .limit(amount)
            .sort(sort)
            .gt(gt)
            .lt(lt)
            .exec((error, result)=>{
                if(error){
                    callback(error, null)
                    console.log('error in FindWhere ', error)
                    return;
                }
                console.log('FindWhere gt lt: ', result)
                callback(null, result)
            })
        })
    }
}


//the class that implements this must define a mongodb schema
export interface MongoController <type>{
    Model(): type
    Schema(): any
    Name(): string
    Index(): any
}

//The class that extends this must have "super(); super.init(this);" in its constructor!
//This is use to gain access to addorupdate, delete, find, etc.. with factory()
export abstract class MongoEntity <type>{
    private controller: MongoController <type>
    private mongo: Mongo
    constructor(){
    }
    init(controller: MongoController <type>, index: any = null){
        this.controller = controller
        this.mongo = new Mongo(this.controller.Name(), this.controller.Schema(), index)
    }
    Objectify(model: type): any{ //converts model to object so it can be stored in db
        const obj = JSON.parse(JSON.stringify(model))
        console.log('Objectify model ', obj)
        return obj
    }
    factory(){
        return this.mongo
    } 
}

//Example of how to use both class above
//PhotoModel is a basic class model
// export class Photo extends MongoEntity<PhotoModel> implements MongoController<PhotoModel> {
//     constructor(){
//         super(); super.init(this, this.Index());
//     }
//     Model(): PhotoModel {
//         const p = new PhotoModel()
//         return p
//     }
//     Index(){
//         return {
//             '$**': 'text' //will index any text field in the schema, use for FindText()
//         }
//     }
//     Schema() {
//         return{
//                _id: {type: String},
//                text: {type: String},
//                markerCoordinates: [{x: Number, y: Number}], //array
//                modifiedDate: Date,
//                createdDate: Date
//         }
//     }
//     Name(): string {
//         return 'photo'
//     }
// }