import * as seq from 'sequelize';
import {EventEmitter} from 'events';
import { isNullOrUndefined } from 'util';
abstract class SQL{
    public Emitter = new EventEmitter();
    public connection: seq;
    public isConnected: boolean;
    public options: any;
    //this creates a connection, might take some time.
    constructor(
        private user: string,
        private password: string,
        private database: string,
        private callback?: ()=>void
    ){
    }

    //this is a virtual method, super reference should not be override
    public define(OptionIn: any){
        this.options = OptionIn
    }
    public connect(){
        this.connection = new seq(this.database, this.user, this.password,this.options);
        this.connection.authenticate().then(()=>{
            console.log('Connection was established to ' + this.options.dialect);
            this.isConnected = true;
            this.Emitter.emit('connect');
            if(this.callback) {
                //console.log('SQL callback');
                this.callback();
            };
        }).catch((err)=>{
            console.log('Unable to connect to MySQL ' + err);
            this.isConnected = false;
        })
    }
    public dispose(){
        this.connection.close();
    }
    //deletes and creates a table, don't use this in production! Use migration instead
    public createTable(model: any, callback?: (model:any)=>void){
        model.sync({force: true})
        .then((e)=>{
            if(callback) {
                //console.log('SQL callback');
                callback(model);
            };
            this.Emitter.emit('createTable', e)})
        .catch((e)=>{
            this.Emitter.emit('createTable', e);
        });
    }
    public ClearTable(model: any){
        model.destroy({
            truncate: true
        })
        this.Emitter.emit('ClearTable');
    }
}

//Note
//creating a new table using sequlize:
//s.createTable(new userModel(s.connection).GetModel());
//Clearing a table:
//s.ClearTable(new Profile(s.connection).GetModel());

export class MySQL extends SQL{
    constructor(
        user: string,
        password: string,
        database: string,
        private server: string,
        private port: number,
        callback?: ()=>void
    ){
        super(user, password, database, callback);
        this.define({ 
            //logging: false,
            host: this.server,
            port: this.port,
            dialect: 'MySQL'
        });
    }
    public define(optionsIn: any){
        super.define(optionsIn)
        super.connect();
    }
}

export class SQLite extends SQL{
    constructor(
        user: string,
        password: string,
        database: string,
        private storage: string,
        callback?: ()=>void,
        typeMemory = false
    ){
        super(user, password, database, callback);
        if(!typeMemory){
            this.define({ 
                storage: this.storage,
                operatorsAliases: false,
                logging: false,
                dialect: 'sqlite'
            });
        }else{
            this.define({ 
                logging: true,
                operatorsAliases: false,
                dialect: 'sqlite'
            });
        }
        
    }
    public define(optionsIn: any){
        console.log("SQlite file: " + this.storage);
        super.define(optionsIn)
        super.connect();
    }
    public static FactoryFastMemory(database: string, callback?: ()=>void){
        return new SQLite(null, null, database, null, callback, true);
    }
}






//DON't USE
//Use for SQLite In-Memory operations. Requires data file also to save and load from
//npm install sqlite3
//Download SQLiteStudio to create and manage database file
//npm install --save sqlite3
const sqlite3 = require('sqlite3').verbose();
export class SQLiteMemory{
    public db:any;
    public dbFile:any;
    private isConnected = false;
    public Emitter = new EventEmitter();
    private sqlite3:any;
    public isFile = false;
    constructor(
        public filePath: string,
        public TableName: string
    ){
        console.log('Connection path ' + filePath)
        this.db = new sqlite3.Database(':memory:', (err) => {
            if (err) {
              return console.error(err.message);
            }else{
                console.log('Connected to the in-memory SQlite database.');
                this.isConnected = true;
                this.Emitter.emit('constructor');
            }
        });
        this.dbFile = new sqlite3.Database(filePath, (e)=>{
            if(e){
                return console.error(e);
            }else{
                console.log('Connected to file database'); 
                this.Emitter.emit('constructorFile');
                this.isConnected = true;
            }
        });
    }
    public createTableHard(DataDefinitions){
        const dropCom = 'DROP TABLE IF EXIST ' + this.TableName;
        const prefixCom = 'CREATE TABLE IF NOT EXISTS ' + this.TableName;
        const command = prefixCom + '(' + DataDefinitions + ')';
        this.SerializedCommand(command, null, ()=>this.Emitter.emit('createTable'));
    }
    public Insert(Verb: string, Value: string){
        const commandInsertSpecific = 'INSERT INTO ' + this.TableName + '('+Verb+')' + ' VALUES(?)';
        this.SerializedCommand(commandInsertSpecific, Value, ()=>this.Emitter.emit('Insert'));
    }
    public Retreive(Verb: string, Value: string, callback:(e,r)=>void){
        const command = 'SELECT ' + Verb + ' FROM ' + this.TableName + ' WHERE ' + Verb + ' = ?';
        if(!this.isFile){
            this.db.each(command,Value, (e,r)=>{
                callback(e,r);
            })
        }else{
            this.dbFile.each(command,Value, (e,r)=>{
                callback(e,r);
            })
        }
    }
    public Delete(Verb: string, Value: string, callback:()=>void){
        const command = 'DELETE FROM ' + this.TableName + ' WHERE ' + Verb + ' = ?';
        console.log(command);
        this.SerializedCommand(command, Value, callback);
    }
    private SerializedCommand(command: string, param: string, callback:()=>void){
        if(!this.isFile){
            this.db.serialize(()=>{
                if(!isNullOrUndefined(param)){
                    this.db.prepare(command).run(param).finalize();   
                }else{
                    this.db.prepare(command).run().finalize();                    
                }
                callback();
            }); 
        }else{
            this.dbFile.serialize(()=>{
                if(!isNullOrUndefined(param)){ 
                    console.log('here' + param);
                    this.dbFile.prepare(command).run(param);   
                }else{
                    this.dbFile.prepare(command).run();                    
                }
                callback();
            }); 
        }
               
    }
    public Close(){
        this.db.close((err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Close the database connection.');
        });

        this.dbFile.close((err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Close the database connection.');
        });
    }
}