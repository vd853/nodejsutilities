
import * as ff from 'fluent-ffmpeg';
import * as fs from 'fs';
import {File} from './File';
import {Image} from './Image'
import {EventEmitter} from 'events';
import {GUID} from './GUID'
import {myMath} from './Data'
import * as hb from 'handbrake-js';
import * as videoshow from 'videoshow';
import * as async from 'async';

export class Encoder{
    public ffmpegExePath: string = '../ffmpeg/bin/ffmpeg.exe' //assume it is in root of any project folder
    private fileName: string;
    private filePath: string;
    public Emitter = new EventEmitter();
    constructor(public AbsoluteFilePath: string, setFF = true){
        if(!fs.existsSync(AbsoluteFilePath)) throw 'video file does not exist';
        this.fileName = File.getFileOnlyOrParentFolderName(this.AbsoluteFilePath);
        this.filePath = File.getPathOnly(this.AbsoluteFilePath);
        console.log('fileName: ' + this.fileName);
        console.log('filePath: ' + this.filePath);
        console.log('ffmpeg path: ', this.ffmpegExePath)
        if(setFF) ff.setFfmpegPath(this.ffmpegExePath); //need to change this for linux
    }

    //access by data.width, data.height, also much more
    getVideoSpec(callback:(err, data)=>void){
        ff.ffprobe(this.AbsoluteFilePath, (err, metadata)=>{
            if (err) {
                callback(err, null);
            } else {
                // console.log('getVideoSpec data ', metadata.streams[0])
                callback(null, metadata.streams[0]);
            }
        });
    }

    screenshot(
        screenshotAbsolutePath: string,
        maxWidth: number,
        counts: number,
        callback?: (response, data)=>void,
        extension: string = '-SS-%s.png', //%00i.png
        fileName?: string)
    {
        let fileNameExludeExtension = this.fileName.substr(0, this.AbsoluteFilePath.length-4);
        if(fileName) fileNameExludeExtension = fileName
        File.createFolder(screenshotAbsolutePath)
        const times = []

        //actual process
        let fileNames: Array<string>
        const proc = new ff(this.AbsoluteFilePath)
        .on('filenames', function(filenames) {
            fileNames = filenames
            console.log('Screenshots created', filenames.length);
        })
        .on('end', function() {
            if(callback){
                callback(null, fileName)
            }
        })
        .on('error', function(err) {
            console.log('an error happened: ' + err.message);
        })
        .screenshots({ 
            // timestamps: times,
            count: counts, 
            size: maxWidth + 'x?',
            filename: fileNameExludeExtension + extension //screenshots names
        }, screenshotAbsolutePath);
    }


    private screenshotRange(
        screenshotAbsolutePath: string,
        maxWidth: number,
        lowSeconds: number,
        highSeconds: number,
        callback?: (response, data)=>void,
        extension: string = '-SS-%s.png', //%00i.png
        fileName?: string)
    {
        if((highSeconds - lowSeconds) < 1 || (highSeconds - lowSeconds) > 400){
            throw ('invalid time range or interval is over 400 seconds')
        }
        let fileNameExludeExtension = this.fileName.substr(0, this.AbsoluteFilePath.length-4);
        if(fileName) fileNameExludeExtension = fileName
        File.createFolder(screenshotAbsolutePath)
        const times: number[] = []
        for(let i = lowSeconds; i < highSeconds + 1; i++){
            times.push(i);
        }
        //actual process
        let fileNames: Array<string>
        const proc = new ff(this.AbsoluteFilePath)
        .on('filenames', function(filenames) {
            fileNames = filenames
            console.log('Screenshots created', filenames.length);
        })
        .on('end', function() {
            if(callback){
                callback(null, fileName)
            }
        })
        .on('error', function(err) {
            console.log('an error happened: ' + err.message);
        })
        .screenshots({ 
            timestamps: times,
            size: maxWidth + 'x?',
            filename: fileNameExludeExtension + extension //screenshots names
        }, screenshotAbsolutePath);
    }

    //create screenshot of the video for every second, resolution is preserved
    screenshotAllSeconds(
        screenshotAbsolutePath: string,
        callback: (err, result)=>void,
        extension: string = '-SS-%s.png', //%00i.png
        fileName?: string
    ){
        this.getVideoSpec(d=>{
            const duration = Math.floor(d.duration);
            myMath.splitRangeByInterval(1, duration, 400, 0, (err, result)=>{
                const splits = result;
                const tasks:any = [];
                splits.forEach(e=>{
                    const task = (callback:(err, result)=>void)=>{
                        console.log('conversion range ', e[0], ':', e[e.length-1]);
                        this.screenshotRange(screenshotAbsolutePath, d.width, e[0], e[e.length-1], (response, data)=>{
                            console.log('a conversion range completed')
                            callback(null, data);
                        })
                    }
                    tasks.push(task);
                })
                async.series(tasks, callback);
            })
        })
    }

    //Converts to playable format on the web of original quality
    //without '/' in the end
    EncodeHB(AbsoluteOutputFilePath: string, extensionName: string){
        if(!fs.existsSync(AbsoluteOutputFilePath)) fs.mkdirSync(AbsoluteOutputFilePath);
        const fileNew = AbsoluteOutputFilePath + '/' + File
        .getFileOnlyOrParentFolderName(this.AbsoluteFilePath)
        .substr(0,this.AbsoluteFilePath.length-3) + "_" + extensionName + ".mp4";
        hb.spawn({
        input: this.AbsoluteFilePath, 
        output: fileNew,
        preset: 'Normal' // Normal and Universal will work on chrome, won't work if not set
        })
        .on('progress', (p)=>{
            this.Emitter.emit('EncodeHB_Progress', p.percentComplete);
        })
    }

    //Set horizontal or vertical to -1 to automatically resize base on aspect ratio
    //AbsoluteOutputFilePath is the output folder, file name will be retain
    //callback is the video file that was created
    //reference is use to keep track of the current process since this method can be call many times in this same class
    //startPosition and duration is use for cropping purpose, otherwise set them to null
    EncodeFF(AbsoluteOutputFolder: string, postfixName: string, horizontal = -1, vertical = -1, startPosition = -1, duration = -1, bitrate=1028, reference='', callback:(err, data)=>void){
        if(duration === -1) duration = 86400; //on day in seconds
        if(startPosition === -1) startPosition = 0;
        let size = '';
        if(horizontal === -1 && vertical !== -1){
            size = '?x' + vertical;
        }
        if(vertical === -1 && horizontal !== -1){
            size = horizontal + 'x?';
        }
        if(vertical !== -1 && horizontal !== -1){
            size = horizontal + 'x' + vertical;
        }
        File.createFolder(AbsoluteOutputFolder)
        //if(!fs.existsSync(AbsoluteOutputFilePath)) fs.mkdirSync(AbsoluteOutputFilePath);
        const fileNameOnly = File.getFileOnlyOrParentFolderName(this.AbsoluteFilePath)
        const fileNew = AbsoluteOutputFolder + '/' + fileNameOnly
        .substr(0,fileNameOnly.length-4) + "_" + postfixName + ".mp4";
        console.log('fileNew ', fileNew)
        const end = ()=>{
            callback(null, fileNew)
        }
        if(size === ''){
            const processor = new ff(this.AbsoluteFilePath) //this one doesn't take stream
            .on('progress', (p)=> {this.Emitter.emit('EncodeFF_Progress', {reference: reference, progress: p.percent});})
            .videoCodec('libx264')
            .audioCodec('libmp3lame')
            .videoBitrate(bitrate)
            .seek(startPosition)
            .setDuration(duration)
            .output(fileNew)
            .on('error', (err, stdout, stderr)=>console.log('Encoding error ', err))
            .on('end', end)
            .run()
        }else{
            const processor = new ff(this.AbsoluteFilePath) //this one doesn't take stream
            .on('progress', (p)=> {this.Emitter.emit('EncodeFF_Progress', {reference: reference, progress: p.percent});})
            .videoCodec('libx264')
            .audioCodec('libmp3lame')
            .videoBitrate(bitrate)
            .seek(startPosition)
            .setDuration(duration)
            .output(fileNew)
            .size(size)
            .on('error', (err, stdout, stderr)=>console.log('Encoding error ', err))
            .on('end', end)
            .run()
        }
    }

    //alternative to gif
    FramesToVideo(outputFile: string, maxWidth: number, imageCount: number, tempFolder: string, callback?:(error, data)=>void){
        if(!tempFolder) tempFolder = '/' + GUID.create()
        File.createFolder(File.getPathOnly(outputFile))
        const screenshotNames = 'screen???.png'
        const processVideo = (response, data)=>{
            const frames:string[] = []
            fs.readdir(tempFolder, (err, list)=>{
                if(err){
                    if(callback){
                        callback(err, null)
                    }
                }
                list.forEach(e=>{
                    frames.push(tempFolder + '/' + e)
                })
                var videoOptions = {
                    fps: 15,
                    loop: 0.1, // seconds 
                    transition: false,
                }
                videoshow(frames, videoOptions)
                .save(outputFile)
                .on('end', ()=>{
                    setTimeout(() => {
                        File.deleteFolder(tempFolder, (result)=>{
                            if(callback) callback(null, {result: outputFile})
                        })
                    }, 1000);
                })
            })
        }
        this.screenshot(tempFolder, maxWidth, imageCount, processVideo, '%00i.png', 'screen')
    }

    //creates a gif files after all the frames are created in a temp folder which will be deleted afterwards
    toGif(outputFile: string, maxWidth: number, imageCount: number, callback?:(error, data)=>void){
        const tempFolder = './' + GUID.create()
        File.createFolder(File.getPathOnly(outputFile))
        const screenshotNames = 'screen???.png'
        const processGif = (response, data)=>{
            Image.pngToGif(tempFolder, screenshotNames, outputFile, ()=>{
                //delete temp folder here
                File.deleteFolder(tempFolder, (result)=>{
                    if(callback) callback(null, {result: 'successful'})
                })
            })
        }
        this.screenshot(tempFolder, maxWidth, imageCount, processGif, '%00i.png', 'screen')
    }
}