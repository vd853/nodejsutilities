import {StringData, Data} from './Data'
import {Replacer} from './formatter';
import * as _ from 'lodash';
import { isNullOrUndefined } from 'util';
import { performance } from 'perf_hooks';

//Takes in a term (or sentence) and compare with many other terms. Produces an array of array<string> which are the words that shows in
//with other words on the same appearence mount
//DON'T USE THIS!
export class MatchExluder{
    public resultArray = new Array<Array<string>>();
    public searchTermArray: Array<string>;
    public searchTermClean: string;
    public patterns = new Map<string, number>(); //key is the pattern, value is the count 
    public patternKeys = new Array<string>();
    public resultTermListOriginal:Array<string>;
    public HighPatterns: Array<string>;
    public Excludables: Array<string>;
    constructor(
        public searchTerm: string, //all terms in here will be excluded in each term of resultTermList
        public resultTermList: Array<string>,
        public callback: (result:Array<string>)=>void,
        public isLowerCase = false,
        public Appearence = 5 //reducing this number means more possible duplicate results
    ){
        this.processSearchTerm();
        this.processResultList();
        //console.log('process result');
        //_.forEach(this.resultArray, e=>console.log(e));
        for(let i = 0; i < this.resultArray.length; i++){
            this.processSingleSearchTermArray(this.resultArray[i], i)            
        }
        this.GetHighPattern();
        this.callback(this.GetExcludableResults());
    }
    private processSearchTerm(){
        if(this.isLowerCase) this.searchTerm = this.searchTerm.toLowerCase();

        // Removes any grammer char and makes searchTerm into an array
        const r = new Replacer(this.searchTerm);
        this.searchTermClean = r.RemovePunctuation().ToString();
        this.searchTermArray = Replacer.StaticToArray(this.searchTerm);
    }

    //Removes any grammer char from resultTermList, creates an array from search results and also exclude words from search terms
    private processResultList(){
        this.resultTermListOriginal = this.resultTermList;
        for(let i = 0; i < this.resultTermList.length; i++){
            const r = new Replacer(this.resultTermList[i]);
            this.resultTermList[i] = r.RemovePunctuation().ToString();
            //console.log(this.resultTermList[i]);
        }
        _.forEach(this.resultTermList, e =>{
            this.resultArray.push(Replacer.StaticToArray(e, this.isLowerCase));
        })        

        for(let i = 0; i < this.resultArray.length; i++){
            this.resultArray[i] = StringData.Except(this.resultArray[i], this.searchTermArray)
        }
    }

    private processSingleSearchTermArray(TermArray: Array<string>, index: number){
        for(let i = 0; i < this.resultArray.length; i++){
            if(i !== index){
                //Get all terms in the result that is not in the current-result-searchterm and exludes any word that is in the current-result-searchterm
                const diff = StringData.Except(StringData.Difference(TermArray, this.resultArray[i]), TermArray);
                if(diff.length > 0){
                    const matchPattern = _.join(diff, ' '); //pattern is a string of words sperated by space as the key
                    if(!_.includes(this.patternKeys, matchPattern)){
                        this.patternKeys.push(matchPattern);
                    }
                }
            }
        }
    }

    //Returns the search term that appears more than this.appearance times
    //First value is apperence amount
    private GetHighPattern(): Array<Array<string>>{
        let concater: Array<string> = [];
        _.forEach(this.patternKeys, e=>{
            concater = _.concat(concater, Replacer.StaticToArray(e));
        });
        const unique = _.uniq(concater);
        this.patterns.clear();
        _.forEach(unique, (e)=>{
            this.patterns[e] = 0;
        });
        _.forEach(concater, (e)=>{
            this.patterns[e]++;
        });

        const HighPatterns = [];
        HighPatterns.push(this.Appearence.toString());
        _.forEach(unique, e =>{
            if(this.patterns[e] > this.Appearence){
                HighPatterns.push(e);
            }
        });
        this.HighPatterns = HighPatterns;
        this.GetExcludableResults();
        return HighPatterns;
    }

    //returns all the given results list that are probably duplicates
    //First value is amount taken out
    public GetExcludableResults() : Array<string>{
        const Excludables = [];
        for(let i = 0; i < this.resultArray.length; i++){
            if(StringData.ContainsAtleast(this.resultArray[i], this.HighPatterns)){
                Excludables.push(this.resultTermListOriginal[i]);
            }
        }
        this.Excludables = Excludables;
        return Excludables;
    }
}

//Returns a list of all minimum possible duplications
//const p = new DuplicateSentence(s1, resultList2, 0, (r)=>console.log(r), true).GetUniqueExclusions();
//DON'T USE THIS!
export class DuplicateSentence{
    private resultants: Array<string>;   
    constructor(
        public ExcludedTerms: string, 
        public PossibleDuplicationList: Array<string>,
        public tolerence = 50, //increase this tolerence for more possible duplications.
        public isLowerCase = false
    ){
            const tol = tolerence/100;
            for(let i = 1; i < 100000; i++){ //increasing i will catch more duplicates
                const m = new MatchExluder(ExcludedTerms, PossibleDuplicationList, ()=>{}, isLowerCase, i);
                const ratio = m.Excludables.length/PossibleDuplicationList.length
                const ratioTest = ratio < tol;
                // console.log('//////////');
                // console.log(m.patterns);
                //console.log(ratio);
                //zero ratio means it cannot find duplicates lower than this, therefore this is the mininum findable
                if(ratio === 0){
                    const m = new MatchExluder(ExcludedTerms, PossibleDuplicationList, ()=>{}, isLowerCase, i-1);
                    this.resultants = m.Excludables;
                    break;
                }
                if(ratioTest){
                    this.resultants = m.Excludables;
                    break;
                }
            }
    }
    public GetExclusions():Array<string>{
        return this.resultants;
    };
    public GetUniqueExclusions():Array<string>{
        return _.uniq(this.resultants);
    }
}

//Given a list of string [PossibleDuplicationList], it will return a list of string that is not similar to a given string [Sentence]
export class DuplicateSentence2{
    private resultants = [];   
    constructor(
        public Sentence: string, 
        public PossibleDuplicationList: Array<string>,
        public tolerence = 50, //increase this tolerence for more possible duplications.
        public isLowerCase = false
    ){
            const tol = tolerence/100;
            const list = [];
            _.forEach(PossibleDuplicationList, (e)=>{
                const c = StringData.PercentDifferenceOfSentence(Sentence, e);
                if(c.percentDifference > tol){
                    this.resultants.push(e);
                }
            })
    }
    public GetExclusions():Array<string>{
        return this.resultants;
    };
    public GetUniqueExclusions():Array<string>{
        return _.uniq(this.resultants);
    }
    public GetNoneExclusion():Array<string>{
        return StringData.Except(this.PossibleDuplicationList, this.GetExclusions());
    }
}

//For [export class DeduplicateSimilar], given a list of strings, it will return a new list with removed possible duplications
//EXAMPLE
// const d = new DeduplicateSimilar(s.GetTitleArray(),['1080p', 'BRRip', '2017'],['720p'], 50, true).GetDeduped();
// console.log(d);
//
// USING THIS LIST
// [ 'Firangi (2017) - 1CD - Desi-Rip - New Source - Hindi - x264 - MP3 - Mafiaking - M2Tv',
//   'My little Pony The Movie 2017 720p BRRip 700 MB - iExTV',
//   'Gerald\'s.Game.2017.720p.WEBRip.2CH.x265.HEVC-PSA',
//   'A.Fantastic.Woman.2017.HD1080P.X264.AAC.Spanish.CHS',
//   'My.Little.Pony.The.Movie.2017.1080p.BluRay.x264-Replica[EtHD]',
//   'My.Little.Pony.The.Movie.2017.720p.BluRay.x264-Replica[EtHD]',
//   'My.Little.Pony.The.Movie.2017.BRRip.XviD.AC3-EVO[EtMovies]',
//   'My.Little.Pony.The.Movie.2017.BDRip.x264-DiAMOND[EtMovies]',
//   'Permanent 2017 Movies 720p HDRip x264 with Sample ☻rDX☻',
//   'Ek Ajnabee (2005) DVDRip - x264 AC3 5.1 - ESub - DTOne',
//   'Forest Fairies 2015 Movies HDRip x264 AAC with Sample ☻rDX☻',
//   'The Stolen 2017 BRRip DD2 0 x264-BDP',
//   'The.Stolen.2017.BRRip.DD2.0.x264-BDP',
//   'Cars.3.2017.BDRip.AC3.ITA-DDV.avi',
//   'The.Disaster.Artist.2017.DVDScr.XVID.AC3.HQ.Hive-CM8[EtMovies]',
//   'Jawaan (2017) - Telugu - WebRip - XviD - 5.1CH (Upmix) - 2CD [Team Jaffa]',
//   'Downsizing.2017.TS.XViD-BL4CKP34RL',
//   'Downsizing.2017.720p.TS.XViD-BL4CKP34RL',
//   'Buckaroo Banzai (1984) .Widescreen 720x300 AVI',
//   'The.Disaster.Artist.2017.DVDScr.XVID.AC3.HQ.Hive-CM8',
//   'Permanent 2017 Eng 1080p WEB-DL x264 [1.4GB] [TorrentCounter]',
//   'Dazed And Confused - Comedy Ben Affleck 1993 Eng Subs 720p [H264-mp4]',
//   'Forest.Fairies.2015.WEBRip.x264-iNTENSO[EtMovies]',
//   'Forest.Fairies.2015.720p.WEBRip.x264-iNTENSO[EtHD]' ]
//
// IT WILL RETURN THIS LIST
// [ 'Firangi (2017) - 1CD - Desi-Rip - New Source - Hindi - x264 - MP3 - Mafiaking - M2Tv',
//   'A.Fantastic.Woman.2017.HD1080P.X264.AAC.Spanish.CHS',
//   'My.Little.Pony.The.Movie.2017.1080p.BluRay.x264-Replica[EtHD]',
//   'Ek Ajnabee (2005) DVDRip - x264 AC3 5.1 - ESub - DTOne',
//   'Forest Fairies 2015 Movies HDRip x264 AAC with Sample ☻rDX☻',
//   'The Stolen 2017 BRRip DD2 0 x264-BDP',
//   'Cars.3.2017.BDRip.AC3.ITA-DDV.avi',
//   'The.Disaster.Artist.2017.DVDScr.XVID.AC3.HQ.Hive-CM8[EtMovies]',
//   'Jawaan (2017) - Telugu - WebRip - XviD - 5.1CH (Upmix) - 2CD [Team Jaffa]',
//   'Downsizing.2017.TS.XViD-BL4CKP34RL',
//   'Buckaroo Banzai (1984) .Widescreen 720x300 AVI',
//   'Permanent 2017 Eng 1080p WEB-DL x264 [1.4GB] [TorrentCounter]',
//   'Forest.Fairies.2015.WEBRip.x264-iNTENSO[EtMovies]' ]
export class DeduplicateSimilar{
    private resultants = [];   
    private removals: Array<{duplication: string, of: string, percentage: number}> = [];
    constructor(
        public List: Array<string>,
        public NoneImportant: Array<string>, //don't consider this during comparison
        public AlwaysExlude: Array<string>, //never include in return list if these word shows up
        public Preference: Array<string>,
        public difference = 40, //increase this tolerence for more possible duplications.
        public isLowerCase = false //if true, comparison will all be done in lower case, return string will be orginal casing
    ){
        const ExcludedList = StringData.ExceptString(this.List, this.AlwaysExlude); //Removes all strings with matching word list in AlwaysExclude
        //console.log('preference: ' + Preference);
        //console.log('ExlcludeList: ' + ExcludedList);
        const diff = difference/100; //tolerence as decimal
        console.log('difference: ' + diff);
        let reList = ExcludedList; //reList will be continousely modified
        let keepChecking = true; //keeps looping while true
        let i = 0; //index to check ExcludeList
        while(keepChecking){
            const remove = []; //List of strings to remove
            //console.log('Checking: ' + ExcludedList[i]);
            for(let j = i+1; j < ExcludedList.length; j++){
                const c = StringData.PercentDifferenceOfSentenceExclude(ExcludedList[i], ExcludedList[j], NoneImportant);
                //console.log('comparing ' + ExcludedList[i] + ' WITH ' + ExcludedList[j]);
                if(c.percentDifference < diff){ //If difference is small enough, it is probably the same string
                    let pushOutItem: string;
                    let keepingItem: string;
                    //if there is a preference, check the clean string array of j if any of its word appears in the preference list
                    //if not then push it out, if it contains then push out i instead
                    if(Preference.length > 0){
                        const cleanString = new Replacer(ExcludedList[j]);
                        const check = StringData.ContainsAtleast(cleanString.RemovePunctuation().ToArray(), Preference)
                        if(check){
                            //console.log('Prefering ' + ExcludedList[j]);
                            pushOutItem = ExcludedList[j];
                            keepingItem = ExcludedList[i];
                        }else{
                            pushOutItem = ExcludedList[i];
                            keepingItem = ExcludedList[j];
                        }
                    }else{
                        pushOutItem = ExcludedList[j];
                        keepingItem = ExcludedList[i];
                    }
                    remove.push(pushOutItem);
                    this.removals.push({duplication: pushOutItem, of:keepingItem, percentage: c.percentDifference});
                    console.log('pushing: ' + pushOutItem + ' difference: ' + c.percentDifference + ' from ' + keepingItem);
                }
            }
            //console.log('NEXT');
            reList = StringData.Except(reList, remove); //use remove list to cut away from reList, which will shorten it
            i++;
            if(i > ExcludedList.length) keepChecking = false; //stop checking if all string from ExcludedList is checked
        }
        this.resultants = reList;
    }
    public GetRemovalReason(): Array<{duplication: string, of: string,  percentage: number}>{
        return this.removals;
    }
    public GetDeduped():Array<string>{ //this list will only contain that of which is not removed
        return this.resultants;
    };
    public GetUniqueDeduped():Array<string>{
        return _.uniq(this.resultants);
    }
    public static GetExclusion(ReasonList: Array<{duplication: string, of: string,  percentage: number}>):Array<string>{
        return _.map(ReasonList, 'duplication'); //StringData.Except(this.List, this.GetDeduped());
    }
}

//if any one sentence from listremovable is similar to any one sentence from list, then that sentence from listremovable will be remove
export class RemoveSimilarityFromB{
    private resultants: Array<string>;   
    private removals: Array<{duplication: string, of: string, percentage: number}> = [];
    constructor(
        public List: Array<string>,
        public ListRemovable: Array<string>,
        public NoneImportant: Array<string>, //don't consider this during comparison
        public AlwaysExlude: Array<string>, //never include in return list if these word shows up
        public difference = 40, //increase this tolerence for more possible duplications.
        public isLowerCase = false //if true, comparison will all be done in lower case, return string will be orginal casing
    ){
        const ExcludedList = StringData.ExceptString(this.ListRemovable, this.AlwaysExlude); 
        const diff = difference/100; //tolerence as decimal
        const ToRemove = [];
        ListRemovable.forEach(e=>{
            let percentage: number;
            let keeper: string;
            const remove = List.some(f=>{
                const c = StringData.PercentDifferenceOfSentenceExclude(e, f, NoneImportant);
                if(c.percentDifference < diff){
                    keeper = f;
                    percentage = c.percentDifference;
                    return true;
                }
            }) 
            if(remove) {
                ToRemove.push(e);
                this.removals.push({duplication: e, of:keeper, percentage: percentage});
                //console.log('pushing: ' + e);                
            }
        })
        this.resultants = ToRemove;
    }
    public GetRemovalReason(): Array<{duplication: string, of: string,  percentage: number}>{
        return this.removals;
    }
    public GetToRemove():Array<string>{
        return this.resultants;
    };
}

//Returns all unique words that appears more than the weighted threshold percent from a give list of sentence. All numbers are excluded.
//Also can return a Map of the word and percent
export class WordRepeated{
    private filteredHighs: Array<string>;
    constructor(private list: Array<string>, private threshold = 20, private isCase = true){
        threshold = threshold/100;
        const uniques = new Map();
        const uniquesObj = new Array<{text: string, percentage: number}>();   
        let max = 0;             
        list.forEach(e=>{
            const r = new Replacer(e);
            const array = r.RemovePunctuation().ToArray();
            array.forEach(f=>{
                if(!StringData.isNumber(f)){
                    let key:string = f;
                    if(!isCase) key = key.toLowerCase();
                    if(!uniques[key]){
                        uniques[key] = 1;
                        uniquesObj.push({text: key, percentage: 0});
                    }else{
                        uniques[key]++;   
                        if(uniques[key] > max){
                            max = _.toNumber(uniques[key]);
                        }              
                    }
                }
            })
        })
        for(let i = 0; i < uniquesObj.length; i++){
            uniquesObj[i].percentage = uniques[uniquesObj[i].text]/max;
        }
        this.filteredHighs = _.filter(uniquesObj, e => e.percentage > threshold);
        console.log(this.filteredHighs);
        console.log('max ' + max + ' treshold ' + threshold);
    }
    public getWords(): Array<string>{
        return this.filteredHighs;
    }
}