"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GoogleMapsAPI = require("googlemaps");
const request = require("request");
const fs = require("fs");
class GoogleMaps {
    constructor(key) {
        this.config = {
            key: key,
            stagger_time: 1000,
            encode_polylines: false,
        };
    }
    //N is positive, W is negative E is positive, resolution is limited to 640
    mapImage2(latitude, longitude, outfile, callback, zoom = 18, horizontal = 640, vertical = 640) {
        const uri = `https://maps.googleapis.com/maps/api/staticmap?size=${horizontal}x${vertical}&maptype=roadmap&markers=color:red%7Clabel:P%7C${latitude},${longitude}&scale=${zoom}&center=${latitude},${longitude}&key=${this.config.key}`;
        if (horizontal > 640 || vertical > 640) {
            console.log('Error, dimension cannot be over 640 ', horizontal, 'x', vertical, ' will continue.');
        }
        //console.log('Google map url ', uri)
        request.head(uri, function (err, res, body) {
            // console.log('content-type:', res.headers['content-type']);
            // console.log('content-length:', res.headers['content-length']);
            request(uri, (err, res, body) => {
            }).pipe(fs.createWriteStream(outfile)
                .on('error', e => {
                callback(e, null);
                console.log('createwritestream error ', e);
            })
                .on('finish', data => {
                callback(null, true);
            }));
        });
    }
    //N is positive, W is negative E is positive
    mapImage(latitude, longitude, outfile, callback, zoom = 18) {
        const coordinates = latitude + ', ' + longitude;
        const gmAPI = new GoogleMapsAPI(this.config);
        const params = {
            center: coordinates,
            zoom: 5,
            size: '1200x1600',
            maptype: 'roadmap',
            markers: [
                {
                    location: coordinates,
                    label: 'Image Location',
                    color: 'red',
                    shadow: true
                }
            ],
            style: [
                {
                    feature: 'road',
                    element: 'all',
                    rules: {
                        hue: '0x00ff00'
                    }
                }
            ]
        };
        gmAPI.staticMap(params); // return static map URL 
        gmAPI.staticMap(params, function (err, binaryImage) {
            if (err) {
                console.log(err);
                callback(err, null);
                return;
            }
            fs.writeFile(outfile, binaryImage, 'binary', (err) => {
                if (err) {
                    console.log(err);
                    callback(err, null);
                    return;
                }
                callback(null, true);
            });
        });
    }
}
exports.GoogleMaps = GoogleMaps;
//# sourceMappingURL=GoogleMaps.js.map