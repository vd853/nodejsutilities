import * as cjs from 'crypto-js';
export class Cipher{
    constructor(private key: string){
    }
    public Encrypt(inputText: string):string {
        return cjs.AES.encrypt(inputText, this.key).toString();
    }
    public Decrypt(inputEncryptedText: string):string{
        return cjs.AES.decrypt(inputEncryptedText, this.key).toString(cjs.enc.Utf8);
    }
    public static Hash(inputText: string): string{
        return cjs.SHA256(inputText).toString();
    }
}
