"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const async = require("async");
const File_1 = require("./File");
class NetworkConfig {
    constructor(configPaths) {
        this.configPaths = configPaths;
    }
    write(field, value, callback) {
        const tasks = [];
        this.configPaths.forEach(e => {
            if (fs.existsSync(e)) {
                const task = (callback) => {
                    this.writePrime(e, field, value, callback);
                };
                tasks.push(task);
            }
            else {
                console.log('WARN: config file does not exist ', e);
            }
        });
        async.parallel(tasks, callback);
    }
    writePrime(filePath, field, value, callback) {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                callback(err + ' or no data', null);
                return;
            }
            // console.log('file data ', data)
            if (!data)
                data = '{}';
            let jData = {};
            try {
                jData = JSON.parse(data);
            }
            catch (e) {
                console.log('error config JSON read ', e);
                jData = {};
            }
            jData[field] = value ? value : 'null';
            fs.writeFile(filePath, JSON.stringify(jData), (err) => {
                if (err) {
                    callback(err, null);
                    return;
                }
                callback(null, jData);
            });
        });
    }
    blank(callback) {
        const tasks = [];
        this.configPaths.forEach(e => {
            if (fs.existsSync(e)) {
                const task = (callback) => {
                    this.blankPrime(e, callback);
                };
                tasks.push(task);
            }
        });
        async.parallel(tasks, callback);
    }
    blankPrime(filePath, callback) {
        fs.writeFile(filePath, JSON.stringify({}), (err) => {
            if (err) {
                if (callback)
                    callback(err, null);
                return;
            }
            if (callback)
                callback(null, true);
        });
    }
    //networkMode is either 'static' or 'dhcp'
    static setSystemNetwork(ipv4, netmask, gateway, networkMode, callback) {
        console.log('centos network settings ', ipv4, netmask, gateway, networkMode);
        if (process.platform === "win32") {
            callback(null, 'Not linux!');
            return;
        }
        ;
        new Promise((resolve, reject) => {
            File_1.File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'NETMASK', netmask, false, (err, result) => {
                if (err)
                    reject(err);
                resolve();
            });
        })
            .catch(err => { callback(null, err); return; })
            .then(() => {
            return new Promise((resolve, reject) => {
                File_1.File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'IPADDR', ipv4, false, (err, result) => {
                    if (err)
                        reject(err);
                    resolve();
                });
            });
        })
            .catch(err => { callback(null, err); return; })
            .then(() => {
            return new Promise((resolve, reject) => {
                File_1.File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'GATEWAY', gateway, false, (err, result) => {
                    if (err)
                        reject(err);
                    resolve();
                });
            });
        })
            .catch(err => { callback(null, err); return; })
            .then(() => {
            return new Promise((resolve, reject) => {
                NetworkConfig.setNetworkMode(networkMode, (err, result) => {
                    if (err)
                        reject(err);
                    resolve();
                });
            });
        })
            .catch(err => { callback(null, err); return; })
            .then(() => {
            callback(null, true);
        });
    }
    static setNetworkMode(networkMode, callback) {
        File_1.File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'BOOTPROTO', networkMode, false, (err, result) => {
            callback(err, result);
        });
    }
    static getNetworkMode(callback) {
        File_1.File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'BOOTPROTO', '', true, (err, result) => {
            callback(err, result);
        });
    }
    static setNetmask(ip, callback) {
        File_1.File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'NETMASK', ip, false, (err, result) => {
            callback(err, result);
        });
    }
    static getNetmask(callback) {
        File_1.File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'NETMASK', '', true, (err, result) => {
            callback(err, result);
        });
    }
    static networkRestart(sudoPassword, callback) {
        if (!System.isWin32()) {
            require('child_process').exec('echo ' + sudoPassword + ' | sudo -S service network restart', function (msg) {
                callback(true);
                return;
            });
        }
        else {
            console.log('Will not restart network on windows machine.');
            callback(false);
        }
    }
}
//fix permission first! sudo chmod -R 777 /etc/sysconfig/network-scripts/ifcfg-eth0
NetworkConfig.networkConfigPathCentos = '/etc/sysconfig/network-scripts/ifcfg-eth0';
exports.NetworkConfig = NetworkConfig;
class System {
    static setHostname(hostname, sudoPassword, callback) {
        if (!System.isWin32()) {
            require('child_process').exec('echo ' + sudoPassword + ' | sudo -S hostnamectl set-hostname ' + hostname, function (msg) {
                callback(msg);
                return;
            });
        }
        else {
            console.log('Will not set hostname on windows machine.');
            callback(false);
        }
    }
    static reboot(sudoPassword, callback) {
        if (!System.isWin32()) {
            require('child_process').exec('echo ' + sudoPassword + ' | sudo -S /sbin/shutdown -r now', function (msg) {
                callback(msg);
                return;
            });
        }
        else {
            console.log('Will not reboot windows machine.');
            callback(false);
        }
    }
    static isWin32() {
        return process.platform === "win32";
    }
}
exports.System = System;
//# sourceMappingURL=Centos.js.map