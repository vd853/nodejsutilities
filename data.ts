import {Replacer} from './formatter';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as fsx from 'fs';


export interface CharIndex{
    matches: Array<number>;
    sequence: Array<number>;
}

export class StringData{
    public static capitalizeEachWord(text: string){
        return text.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
    public static isNumber(text: string){
        const reg = new RegExp(/^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$/);
        return reg.test(text);
    }
    //returns all number seen in a string. A number will be seperate by any char. Includes negative numbers.
    public static ExtractNumber(text: string): Array<number>{
        const broken: string | RegExpMatchArray | null = text.match(/-?\d+/g);
        const number: number[] = [];
        if(broken === null){
            return [];
        }
        broken.toString().split(',').forEach(e=>number.push(_.toNumber(e)));
        return number;
    }

    public static isIp(text: string):boolean{
        if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(text)) {  
            return true 
        }  
        return false
    }
    //returns the percent difference between sentence1 and 2. Higher percentdifference means they are less likely to match
    public static PercentDifferenceOfSentence(Sentence1: string, Sentence2:string): 
    {percentDifference: number, difference1: number, difference2: number}{
        const A = this.CleanSentencesToArray(Sentence1, Sentence2);
        const cA = this.getCompareArray(A.A1, A.A2);
        let per = (cA.array1.length + cA.array2.length)/(A.A1.length + A.A2.length);       
        //console.log('Percent Difference: ' + per.toFixed(2)); 
        return {percentDifference: per, difference1: cA.array1.length, difference2: cA.array2.length};
    }

    //returns difference words of each array with length of differences. Also return the total of each array 
    private static getCompareArray(ArrayCompare1: Array<string>, ArrayCompare2: Array<string>):
    {array1: Array<string>, array2: Array<string>}
    {
        
        const difference1:Array<string> = _.difference(ArrayCompare1, ArrayCompare2);
        const difference2:Array<string> = _.difference(ArrayCompare2, ArrayCompare1);
        return {array1: difference1, array2: difference2}; 
    }

    //Removes anything from 1 that appears in 2
    //ex: StringData.Difference(['a', 'b', 'c', 'd'], ['a', 'd', 'c']) ==> [ 'b' ]
    public static Difference(ArrayCompare1: Array<string>, ArrayCompare2: Array<string>): Array<string>
    {
        
        const difference1:Array<string> = _.difference(ArrayCompare1, ArrayCompare2);
        const difference2:Array<string> = _.difference(ArrayCompare2, ArrayCompare1);
        return _.union(difference1, difference2);
        // if(difference1.length > difference2.length){
        //     return difference1;
        // }else{
        //     return difference2;
        // }
    }
    public static Contains(ArrayCompare1: Array<string>, ArrayCompare2: Array<string>): Array<string>
    {
        return _.intersection(ArrayCompare1, ArrayCompare2);
    }

    //return true if at least 1 member in ArrayCompare2 is in ArrayCompare1
    public static ContainsAtleast(ArrayCompare1: Array<any>, ArrayCompare2: Array<any>): boolean
    {
        return _.difference(ArrayCompare1, ArrayCompare2).length < ArrayCompare1.length;
    }
    //returns true when all element from ArrayCompare2 must be in ArrayCompare1
    public static ContainsAll(ArrayCompare1: Array<any>, ArrayCompare2: Array<any>): boolean
    {
        return !ArrayCompare2.some(e=>{
            if(!_.includes(ArrayCompare1, e)){
                return true;
            }
        })
    }
    public static Except(orginial: Array<string>, exclude: Array<string>): Array<string>
    {
        return _.difference(orginial, exclude);
    }

    //Takes a list of strings and removes all the entries that contains the words in the exclude list
    public static ExceptString(orginial: Array<string>, exclude: Array<string>): Array<string>
    {
        if(!orginial || !exclude) return [];
        const list: string[] = [];
        _.forEach(orginial,(e)=>{
            const r = new Replacer(e);
            const array = Replacer.StaticToArray(r.RemovePunctuation().ToString());
            if(!this.ContainsAtleast(array, exclude)){
                list.push(e);
            }
        })
        return list;
    }

    //Returns any unique value of the second array. Does not include any values from the first
    public static UniqueInSecond<T>(First: Array<T>, Second: Array<T>): Array<T>{
        let list: T[] = [];
        Second.forEach(e=>{
            let isUnique = true;
            First.forEach(f=>{  
                let marked = false; 
                if(e === f){
                    if(isUnique && !marked){
                        isUnique = false;
                        marked = true;
                    }
                }
            })
            if(isUnique) list.push(e);
        })
        list = _.uniq(list);
        return list;
    }

    public static CleanSentencesToArray(Sentence1: string, Sentence2: string): any{
        const r = new Replacer(Sentence1);             
        const RemovedPunc1 = r.RemovePunctuation().ToString();
        const RemovedPunc2 = r.ReplaceEntire(Sentence2).RemovePunctuation().ToString();
        const ArrayCompare1 =  RemovedPunc1.split(' ');
        const ArrayCompare2 =  RemovedPunc2.split(' ');
        return {A1: ArrayCompare1, A2: ArrayCompare2};
    }

    //Same as PercentDifferenceOfSentence but you can add a list of words to exclude from percentage, 
    //Exclude DOES NOT exclude the sentence entirely
    //this could cause more differences since you are excluding from both sentence
    //or could cause less differences if you are excluding from just one sentence 
    public static PercentDifferenceOfSentenceExclude(Sentence1: string, Sentence2: string, Exclude: Array<string>): 
    {percentDifference: number, difference1: number, difference2: number}{
        const A = this.CleanSentencesToArray(Sentence1, Sentence2);
        const ArrayCompare1 = _.difference(A.A1, Exclude);
        const ArrayCompare2 = _.difference(A.A2, Exclude);
        const cA = this.getCompareArray(ArrayCompare1, ArrayCompare2);        
        let per = (cA.array1.length + cA.array2.length)/(ArrayCompare1.length + ArrayCompare2.length);      
        return {percentDifference: per, difference1: cA.array1.length, difference2: cA.array2.length};
    }

    //Returns all position that matches char in text with start sequence position of char with sequencelimit
    public static FindIndexOfCharSequence<CharIndex>(text: string, char: string, sequenceLimit: number): 
    {Matches: Array<number>, SequencePoint: Array<number>}{
        const indexes:Array<number> = []; //for all target
        const indexesSequence:Array<number> = []; //for starting point of all target sequence
        let count = 0;
        let countNextLimit = 0;
        for(let i = 0; i < text.length; i++){
            if(text[i] === char){
                count++;
                indexes.push(i);
                if(count > countNextLimit+sequenceLimit-1){
                    const l = i-sequenceLimit+1;
                    indexesSequence.push(l);
                    countNextLimit = l + sequenceLimit;
                    count = countNextLimit;
                }
            }else{
                count=countNextLimit;
            }
        }
        return {Matches: indexes, SequencePoint: indexesSequence};
    }

    //Finds all number that is to the right of the marker that is contain in a text. Many markers can exist.
    //if isPostive, it will check the front of the marker
    //jfkpass432lepass4544passjfkepass23123lsjf => [432, 4544, 23123]) of the marker pass
    public static NumbersAtMarkerPositions(text: string, marker: string, isPositive = true): Array<number>{
        const regexpPositive = new RegExp('(?:'+marker+')[0-9]+', 'gi');
        const regexpNegative = new RegExp('[0-9]+' + marker, 'gi');
        let matches: RegExpMatchArray | null = [];
        if(isPositive){
            matches = text.match(regexpPositive);
        }else{
            matches = text.match(regexpNegative);
        }
        const matchesExcluded:Array<number> = [];
        _.forEach(matches, e=>matchesExcluded.push(_.toNumber(e.replace(marker, ''))));
        return matchesExcluded;
    }

    public static GetStatsNumberAtMarkerPositionByList(list: Array<string>, marker: string, isPositive): {min: number, max: number}{
        const workList: number[] = [];
        list.forEach(e=>{
            const value = this.NumbersAtMarkerPositions(e, marker, isPositive)[0];
            if(value) workList.push(value);
        })
        return {min: _.min(workList), max: _.max(workList)};
    }

    //If a marker is contain in two similar sentence, check if the numeric value next to it is increaing or decreaing. If isPositive the marker will be on the right
    //checkNumber is use for starting point of increment, it is optional, set -1 to use Sentence1 instead
    public static SentenceIsSequence(Sentence1: string, Sentence2: string, marker: string, isPositive = true, isIncreasing = true, checkNumber = -1): Boolean{        
        //console.log('s1 ' + Sentence1 + ' s2 ' + Sentence2);
        if(!Sentence1.includes(marker) || !Sentence2.includes(marker)) return false;
        const NumbersOfS1 = this.NumbersAtMarkerPositions(Sentence1, marker, isPositive);
        const NumbersOfS2 = this.NumbersAtMarkerPositions(Sentence2, marker, isPositive);
        //One of the sentence must contain a value at the marker        
        if(NumbersOfS1.length < 1 || NumbersOfS2.length < 1) return false;
        // const tor = Threshold/100;  
        // console.log('Threshold ' + tor);
        //if(_.toNumber(this.PercentDifferenceOfSentenceExclude(Sentence1, Sentence2, [marker]).percentDifference) > tor) return false; 
        //Test if the number at markers are increaing by checkNumber
        if(checkNumber !== -1){
            console.log(checkNumber + ':' + NumbersOfS2[0]);
            if(isIncreasing){
                if(checkNumber < NumbersOfS2[0]) return true;
            }else{                
                if(checkNumber > NumbersOfS2[0]) return true;
            }
            return false;
        }

        //Test if the numbers at markers are increaing or decreasing
        if(isIncreasing){
            if(NumbersOfS1[0] < NumbersOfS2[0]) return true;
        }else{
            if(NumbersOfS1[0] > NumbersOfS2[0]) return true;
        }
        return false;
    }


    //Given a list of any sentence, return a list of all the sentence that is in sequence with Sentence
    public static GetListOfSentenceInSequence(Sentence: string, List: Array<string>, marker: string, 
        isPositive = true, isIncreasing = true, checkNumber = -1): 
        {ofSequences:Array<string>, max: number}
    {
        let returnList: string[] = [];
        let maxChecker = []; //list of all values to find the max
        _.forEach(List, (e)=>{
            if(this.SentenceIsSequence(Sentence, e, marker, isPositive, isIncreasing, checkNumber)){
                returnList.push(e);
                maxChecker = _.concat(maxChecker, this.NumbersAtMarkerPositions(e, marker, isPositive));
            }
        })
        returnList = StringData.ExceptString(returnList, [Sentence]);
        return {ofSequences: returnList, max:_.max(maxChecker)};
    }

    public static PrintArrayOfStringArray<ArrayType>(list: Array<ArrayType>){
        _.forEach(list, l => console.log(l));
    }

    //Returns a list given depending on it's sequence direction and marker based on the [Sentence]
    //EXAMPLE, given
    //         const main = 'Live.PD.Police.Patrol.S02E04.WEB.h264-TBS[ettv]'
    //         const list = [
    //         'DARK.S01.NF.WEBRip.AAC2.0.x264-STRiFE[ettv]',
    //         'Black.Mirror.SEASON.04.S04.COMPLETE.720p.10bit.WEBRip.2CH.x265.HEVC-PSA',
    //         'Live.PD.Police.Patrol.S02E01.WEB.h264-TBS[ettv]',
    //         'Live.PD.Police.Patrol.S02E02.WEB.h264-TBS[ettv]',
    //         'Live.PD.Police.Patrol.S02E04.WEB.h264-TBS[ettv]',
    //         'Live.PD.Police.Patrol.S02E03.WEB.h264-TBS[ettv]',
    //         'Winters.Weirdest.Events--BBC-2017-720p-w.subs-x265-HEVC.mp4',
    //         'Tales of Tomorrow - Frankenstein S1 ep.16',
    //         'Black Mirror S04 Complete 1080p HD x264 [4.6GB] [Season 4 Full]',
    //         'UFC 219 Early Prelims 720p WEB-DL H264 Fight-BB'];
    //         const c = StringData.GetSequenceList(main, list, 'S02E', true, false);
    //         console.log(c);
    //OUTPUT:
    //          [ 'Live.PD.Police.Patrol.S02E01.WEB.h264-TBS[ettv]',
    //          'Live.PD.Police.Patrol.S02E02.WEB.h264-TBS[ettv]',
    //          'Live.PD.Police.Patrol.S02E03.WEB.h264-TBS[ettv]' ]

    public static GetSequenceList(Sentence: string, List: Array<string>, marker: string, isPositive = true, isIncreasing = true, checkNumber = -1): {Sequence: Array<string>, NoneSequence: Array<string>}{
        const returnList: string[] = [];
        _.forEach(List, (e)=>{
            if(e !== Sentence){
                const test = this.SentenceIsSequence(Sentence, e, marker, isPositive, isIncreasing, checkNumber);              
                if(test){
                    returnList.push(e);
                    //console.log('Pushing ' + e);
                } 
            }
        })
        return {Sequence: returnList, NoneSequence: StringData.Difference(List, returnList)};
    }
}
export class List{
    //Returns array objects of certein depth base on the number of DepthNames. TypeOut is the new return array type
    //Example: array of class test1 has member of array class test2, array class test2 has a member of array class test3
    //DepthMap(test1, ['test2', 'test3']) will return an array of class test3
    public static DepthMap<typeOut>(List: Array<any>, DepthNames: Array<string>): Array<typeOut>{
        let relister = List;
        for(let i = 0; i < DepthNames.length; i++){
            //console.log(DepthNames[i]);
            relister = _.map(relister, DepthNames[i]);
            //console.log(relister);            
        }
        for(let i = 0; i < relister.length; i++){
            relister[i] = <typeOut>relister[i];
        }
        return relister;
    }
}
export class myMath{
    public static getNumberDigitCount(input: number):number{
        return Math.trunc(Math.log10(input)+1);
    }
    public static isEven(val: number): boolean{
        return (val%2 == 0);
    }
    public static mean(numbers: Array<number>): number{
        let sum = 0
        numbers.forEach(e=>{
            sum+=e
        })
        return sum/numbers.length
    }

    //returns an array with upper and lower bound split into different array by  the set interval
    //offset will shift the upper and lower bound
    public static splitRangeByInterval(lowerBound: number, upperBound: number, interval: number, offset: number, callback:(err, result)=>void){
        if(lowerBound > upperBound){
            callback('Lower bound cannot be higher than upper bound', null)
            return;
        }
        let count = 0;
        const ranges: number[][] = [];
        let currentRange: number[] = []

        for(let i = lowerBound + offset; i < upperBound+1+offset; i++){
            currentRange.push(i);
            count++;
            if(count > interval){
                ranges.push(currentRange);
                currentRange = []
                count = 0;
            }
        }
        if(currentRange.length > 0){
            ranges.push(currentRange)
        }

        callback(null, ranges);
    }
}
export class Data{
    public static GetMapKey(map, value){
        return Object.keys(map).find(key => map[key] === value);
    }
    public static JSONObjectBack(object){
        return JSON.parse(JSON.stringify(object))
    }
    public static JSONFromFile(path: string, callback:(err, data)=>void){
        fsx.readFile(path, 'utf8', function (err, data) {
            if (err){
                callback(err, null)
                return
            }
            callback(null, JSON.parse(data))
        });
    }
}

export class myDate{
    static getPreviousWorkday(today?: Date){ //today can be anyday
        let thisToday: Date
        if(today){
            thisToday = moment(today).add(1, 'd').toDate()
        }else{
            thisToday = moment(new Date()).add(1, 'd').toDate()
        }
        return [1, 2, 3, 4, 5].indexOf(moment(thisToday).subtract(1, 'day').day()) > -1 ? 
        moment(thisToday).subtract(1, 'day').toDate() : moment(moment(thisToday).day(-2).toDate());
      }
}
