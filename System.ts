import { spawn, exec } from 'child_process'; 
import * as opn from 'opn';
import {File} from './File';
export class System{

    //Gets all the argument pass in from the CLI when node runs a js with args, example "node myscript myparam1 myparam2"
    public static CaptureCLIArugments(): Array<{index: number, arg: string}>{
        const args: Array<{index: number, arg: string}> = [];
        process.argv.forEach(function (val, index, array) {
            if(index > 1){
                args.push({index: index-2, arg: val});
            }
        });
        return args
    }

    //can't get the cwd option to work. consider putting cd PATH in the bat or make a bat file generator...
    public static Run(file: string){        
        const b = spawn(file);
        b.stdout.on('data', (d)=>{
            console.log('FROM: ' + file + '\n' + d.toString());
        })
    }
    public static Open(file: string){
        opn(file);
    }
}