"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const seq = require("sequelize");
const events_1 = require("events");
const util_1 = require("util");
class SQL {
    //this creates a connection, might take some time.
    constructor(user, password, database, callback) {
        this.user = user;
        this.password = password;
        this.database = database;
        this.callback = callback;
        this.Emitter = new events_1.EventEmitter();
    }
    //this is a virtual method, super reference should not be override
    define(OptionIn) {
        this.options = OptionIn;
    }
    connect() {
        this.connection = new seq(this.database, this.user, this.password, this.options);
        this.connection.authenticate().then(() => {
            console.log('Connection was established to ' + this.options.dialect);
            this.isConnected = true;
            this.Emitter.emit('connect');
            if (this.callback) {
                //console.log('SQL callback');
                this.callback();
            }
            ;
        }).catch((err) => {
            console.log('Unable to connect to MySQL ' + err);
            this.isConnected = false;
        });
    }
    dispose() {
        this.connection.close();
    }
    //deletes and creates a table, don't use this in production! Use migration instead
    createTable(model, callback) {
        model.sync({ force: true })
            .then((e) => {
            if (callback) {
                //console.log('SQL callback');
                callback(model);
            }
            ;
            this.Emitter.emit('createTable', e);
        })
            .catch((e) => {
            this.Emitter.emit('createTable', e);
        });
    }
    ClearTable(model) {
        model.destroy({
            truncate: true
        });
        this.Emitter.emit('ClearTable');
    }
}
//Note
//creating a new table using sequlize:
//s.createTable(new userModel(s.connection).GetModel());
//Clearing a table:
//s.ClearTable(new Profile(s.connection).GetModel());
class MySQL extends SQL {
    constructor(user, password, database, server, port, callback) {
        super(user, password, database, callback);
        this.server = server;
        this.port = port;
        this.define({
            //logging: false,
            host: this.server,
            port: this.port,
            dialect: 'MySQL'
        });
    }
    define(optionsIn) {
        super.define(optionsIn);
        super.connect();
    }
}
exports.MySQL = MySQL;
class SQLite extends SQL {
    constructor(user, password, database, storage, callback, typeMemory = false) {
        super(user, password, database, callback);
        this.storage = storage;
        if (!typeMemory) {
            this.define({
                storage: this.storage,
                operatorsAliases: false,
                logging: false,
                dialect: 'sqlite'
            });
        }
        else {
            this.define({
                logging: true,
                operatorsAliases: false,
                dialect: 'sqlite'
            });
        }
    }
    define(optionsIn) {
        console.log("SQlite file: " + this.storage);
        super.define(optionsIn);
        super.connect();
    }
    static FactoryFastMemory(database, callback) {
        return new SQLite(null, null, database, null, callback, true);
    }
}
exports.SQLite = SQLite;
//DON't USE
//Use for SQLite In-Memory operations. Requires data file also to save and load from
//npm install sqlite3
//Download SQLiteStudio to create and manage database file
//npm install --save sqlite3
const sqlite3 = require('sqlite3').verbose();
class SQLiteMemory {
    constructor(filePath, TableName) {
        this.filePath = filePath;
        this.TableName = TableName;
        this.isConnected = false;
        this.Emitter = new events_1.EventEmitter();
        this.isFile = false;
        console.log('Connection path ' + filePath);
        this.db = new sqlite3.Database(':memory:', (err) => {
            if (err) {
                return console.error(err.message);
            }
            else {
                console.log('Connected to the in-memory SQlite database.');
                this.isConnected = true;
                this.Emitter.emit('constructor');
            }
        });
        this.dbFile = new sqlite3.Database(filePath, (e) => {
            if (e) {
                return console.error(e);
            }
            else {
                console.log('Connected to file database');
                this.Emitter.emit('constructorFile');
                this.isConnected = true;
            }
        });
    }
    createTableHard(DataDefinitions) {
        const dropCom = 'DROP TABLE IF EXIST ' + this.TableName;
        const prefixCom = 'CREATE TABLE IF NOT EXISTS ' + this.TableName;
        const command = prefixCom + '(' + DataDefinitions + ')';
        this.SerializedCommand(command, null, () => this.Emitter.emit('createTable'));
    }
    Insert(Verb, Value) {
        const commandInsertSpecific = 'INSERT INTO ' + this.TableName + '(' + Verb + ')' + ' VALUES(?)';
        this.SerializedCommand(commandInsertSpecific, Value, () => this.Emitter.emit('Insert'));
    }
    Retreive(Verb, Value, callback) {
        const command = 'SELECT ' + Verb + ' FROM ' + this.TableName + ' WHERE ' + Verb + ' = ?';
        if (!this.isFile) {
            this.db.each(command, Value, (e, r) => {
                callback(e, r);
            });
        }
        else {
            this.dbFile.each(command, Value, (e, r) => {
                callback(e, r);
            });
        }
    }
    Delete(Verb, Value, callback) {
        const command = 'DELETE FROM ' + this.TableName + ' WHERE ' + Verb + ' = ?';
        console.log(command);
        this.SerializedCommand(command, Value, callback);
    }
    SerializedCommand(command, param, callback) {
        if (!this.isFile) {
            this.db.serialize(() => {
                if (!util_1.isNullOrUndefined(param)) {
                    this.db.prepare(command).run(param).finalize();
                }
                else {
                    this.db.prepare(command).run().finalize();
                }
                callback();
            });
        }
        else {
            this.dbFile.serialize(() => {
                if (!util_1.isNullOrUndefined(param)) {
                    console.log('here' + param);
                    this.dbFile.prepare(command).run(param);
                }
                else {
                    this.dbFile.prepare(command).run();
                }
                callback();
            });
        }
    }
    Close() {
        this.db.close((err) => {
            if (err) {
                return console.error(err.message);
            }
            console.log('Close the database connection.');
        });
        this.dbFile.close((err) => {
            if (err) {
                return console.error(err.message);
            }
            console.log('Close the database connection.');
        });
    }
}
exports.SQLiteMemory = SQLiteMemory;
//# sourceMappingURL=SQL.js.map