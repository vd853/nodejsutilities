import * as synaptic from 'synaptic'
import * as fs from 'fs'
export class NeuralNetwork{
    trainingSet:Array<{input: Array<number>, output: Array<number>}> = []
    iterations = Math.pow(10, 6)
    error: number
    perceptron: any;
    schedule: any;
    options: any
    result: any;
    private scaler = 1; //this can be consider high
    constructor(
        private inputSize: number, // your raw input and output values should be scaled similarly for better results
        private outputSize: number,
        private hiddenSize: number, // setting this too high will result in overfitting
        private errorReduction = 4,
        private log = true
    ){
        this.error = 1*Math.pow(10, -errorReduction)
        //hidden neuron size are currently hardcoded
        this.perceptron = new synaptic.Architect.Perceptron(inputSize,hiddenSize,hiddenSize,outputSize) //no need to do more than 2 hidden layer for most situtation
        this.schedule = {
            every: 500, 
            do: (data) => {
                if(this.log){
                    console.log("error", data.error, "iterations", data.iterations, "rate", data.rate);
                }
            }
        }
        this.options = {
            rate: 0.001,
            iterations: this.iterations,
            //shuffle: true,
            error: this.error,
            //cost: synaptic.Trainer.cost.MSE,
            schedule: this.schedule
        }
    }
    pushTrainigSetUnit(input: Array<number>, output: Array<number>){
        if(input.length !== this.inputSize){
            throw new Error('You are pushing an input array of the wrong size')
        }
        if(output.length !== this.outputSize){
            throw new Error('You are pushing an output array of the wrong size')
        }
        //scaler will be use to normal between training and activation
        input.forEach(element => {
            if(element > this.scaler) this.scaler = element
        });
        output.forEach(element => {
            if(element > this.scaler) this.scaler = element
        });

        this.trainingSet.push({input: input, output: output})
    }
    Start(callback:(results:any)=>void){
        //console.log('training set ', this.trainingSet)
        //console.log('option ', this.options)
        const trainer = new synaptic.Trainer(this.perceptron)
        this.result = trainer.train(this.scaledData(), this.options)
        callback(this.result)
    }
    private scaledData(){
        const trainingSetScaled:Array<{input: Array<number>, output: Array<number>}> = []
        this.trainingSet.forEach(e=>{
            const input = []
            const output = []
            e.input.forEach(i=>{
                input.push(i/this.scaler)
            })
            e.output.forEach(o=>{
                output.push(o/this.scaler)
            })
            trainingSetScaled.push({input: input, output: output})
        })
        if(this.log)console.log('scaled training set ', trainingSetScaled)
        return trainingSetScaled
    }
    save(file: string, callback?:(d)=>void){
        if(this.result){
            fs.writeFile(file, JSON.stringify(this), callback)
        }
    }
    load(file: string, callback?:(d)=>void){
        fs.readFile(file, 'utf8', (err, data)=>{
            if(err){
                callback(err)
                return;
            }else{
                const Jthis = JSON.parse(data)
                this.error = Jthis.error
                this.inputSize = Jthis.inputSize
                this.iterations = Jthis.iterations
                this.log = Jthis.log
                this.options = Jthis.options
                this.outputSize = Jthis.outputSize
                this.result = Jthis.result
                this.perceptron = synaptic.Architect.Perceptron.fromJSON(Jthis.perceptron)
                callback(this.perceptron)
            }
        })
    }
    activate(input: Array<number>):Array<number>{
        const eachOneMulti = (array, scale)=>{
            const r = []
            array.forEach(element => {
                r.push(element*scale)
            });
            //console.log('multi ', r)
            return r;
        }
        const eachOneDiv = (array, scale)=>{
            const r = []
            array.forEach(element => {
                r.push(element/scale)
            });
            //console.log('divide ', r)
            return r;
        }
        const result = this.perceptron.activate(eachOneDiv(input, this.scaler))
            return eachOneMulti(result, this.scaler)
        if(input.length === this.inputSize){
            const result = this.perceptron.activate(eachOneDiv(input, this.scaler))
            return eachOneMulti(result, this.scaler)
        }else{
            throw new Error(input.length + ' is the wrong length, correct length is ' + this.inputSize)
        }
    }
    
}