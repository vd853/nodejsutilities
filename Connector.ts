//creates a quick connection to the local sqlite file assuming it's db name is dbMain
import {SQLite} from './SQL';
export class Connector{
    constructor(callback: (c:SQLite)=>void, database = 'dbMain'){
        const con = new SQLite(null, null, database, database+'.db', ()=>{
            callback(con); //returns the class, not connection
        })
    }
    public static factory(callback: (c:SQLite)=>void){
        const cc = new Connector(callback);
    }
}