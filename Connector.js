"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//creates a quick connection to the local sqlite file assuming it's db name is dbMain
const SQL_1 = require("./SQL");
class Connector {
    constructor(callback, database = 'dbMain') {
        const con = new SQL_1.SQLite(null, null, database, database + '.db', () => {
            callback(con); //returns the class, not connection
        });
    }
    static factory(callback) {
        const cc = new Connector(callback);
    }
}
exports.Connector = Connector;
//# sourceMappingURL=Connector.js.map