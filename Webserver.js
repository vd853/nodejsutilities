"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const exphbs = require("express-handlebars");
const expressValidator = require("express-validator");
const expressSession = require("express-session");
const request = require("request");
const favicon = require("serve-favicon");
const fs = require("fs");
const lusca = require("lusca");
// const SQLiteStore = require('connect-sqlite3')(expressSession);
// npm install passport-local
// npm install passport
// npm install express-handlebars
// npm install body-parser
// npm install express --save
// npm install express-session
// npm install express-validator
class ExpressObj {
    constructor(port = 1234, HTMLEntry = '', faviconPath = '', staticFolder = '', CORS, SSL) {
        this.port = port;
        this.SSL = SSL;
        this.isRunning = false;
        let options = {};
        if (this.SSL) {
            options = {
                key: this.SSL.key,
                cert: this.SSL.cert,
                passphrase: this.SSL.passphrase
            };
        }
        this.app = express();
        if (this.SSL) {
            this.server = require('https').Server(options, this.app);
        }
        else {
            this.server = require('http').Server(this.app);
        }
        this.io = require('socket.io').listen(this.server);
        this.HTMLEntry = HTMLEntry;
        this.faviconPath = faviconPath;
        this.staticFolder = staticFolder;
        if (CORS === null || CORS === undefined) {
            this._CORS = CORSConfig.factory();
        }
        else {
            this._CORS = CORS;
        }
        this.initMW();
        this.staticHTML();
        this.initSession();
        this.initLusca();
        this.initHandleBars();
        this.setCORS();
    }
    Start(callback) {
        this.server.listen(this.port, () => {
            if (callback)
                callback();
            this.isRunning = true;
            console.log('Express running on port ' + this.port);
        });
    }
    Stop() {
        if (this.isRunning) {
            this.app.close();
            console.log('Express closed on port ' + this.port);
        }
        else {
            console.log('Express is not running or did not yet start on port ' + this.port + ' cannot be close.');
        }
    }
    static getNgDistPath(projFolder) {
        return '../../' + projFolder + '/dist';
    }
    initMW() {
        this.app.use(bodyParser.json({ limit: '50mb' })); //need this to use res.json....
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(expressValidator());
    }
    staticHTML() {
        if (this.faviconPath !== '') {
            if (!fs.existsSync(this.faviconPath)) {
                throw 'favicon doesnt exist';
            }
            console.log('a front end build is being used with favicon: ', this.faviconPath);
            this.app.use(favicon(this.faviconPath));
        }
        if (this.HTMLEntry !== '') {
            console.log('a front end build is being used: ', this.HTMLEntry);
            this.app.use(express.static(this.HTMLEntry));
        }
        if (this.staticFolder !== '') {
            console.log('serving static files: ', this.staticFolder);
            if (!fs.existsSync(this.staticFolder))
                console.log('Warning static folder does not exist!');
            this.app.use(express.static(this.staticFolder));
        }
    }
    initLusca() {
        this.app.use(lusca({
            csp: {
                policy: {
                    'default-src': '\'self\'',
                    'img-src': '\'self\' data:'
                }
            },
            xframe: 'SAMEORIGIN',
            p3p: 'ABCDEF',
            hsts: { maxAge: 31536000, includeSubDomains: true, preload: true },
            xssProtection: true,
            nosniff: true,
            referrerPolicy: 'same-origin'
        }));
    }
    initSession() {
        if (this._CORS.sessionStore === SessionStore.MongoDB) {
            const MongoDBStore = require('connect-mongodb-session')(expressSession);
            const storeMongoDb = new MongoDBStore({
                uri: 'mongodb://127.0.0.1/maindb',
                databaseName: 'maindb',
                collection: 'expressSessions'
            });
            const optionsMongoDB = {
                secret: 'This is a secret',
                resave: true,
                saveUninitialized: false,
                rolling: true,
                cookie: {
                    maxAge: this._CORS.sessionExpire //in ms
                },
                store: storeMongoDb,
            };
            this.app.use(require('express-session')(optionsMongoDB));
            console.log('Session options ', optionsMongoDB);
        }
        if (this._CORS.sessionStore === SessionStore.NonDB) {
            this.app.use(expressSession({
                secret: 'moo',
                resave: false,
                saveUninitialized: true,
                cookie: {
                    secure: false,
                    maxAge: this._CORS.sessionExpire //in ms
                },
                rolling: true //maintains session if active
            }));
        }
        if (this._CORS.sessionStore === SessionStore.none) {
            //session feature will simply not exist
        }
    }
    initHandleBars() {
        if (this.HTMLEntry !== '')
            return;
        //your page should be this folder ./webserver.handlebars ,note it will bypass the layout folder
        this.app.engine('handlebars', exphbs({ defaultLayout: 'Webserver' })); //name of your main .handlebars file
        this.app.set('view engine', 'handlebars');
        this.app.set('views', __dirname);
        this.app.get('/', function (req, res) {
            res.render('webserver', { layout: false, body: '<p>Server is working using handlebar.</p>' });
        });
    }
    setCORS() {
        this.app.use((req, res, next) => {
            let origin = this._CORS.selfSite === '*' ? req.headers.origin ? req.headers.origin : '*' : '*';
            // console.log('origin ', origin)
            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin', origin); //your website
            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', this._CORS.methods);
            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
            // Set to true if you need the website to include cookies in the requests sent
            // to the API (e.g. in case you use sessions)
            res.setHeader('Access-Control-Allow-Credentials', true);
            // Pass to next layer of middleware
            next();
        });
        this.app.all('*', (req, res, next) => {
            console.log('session id ALL ', req.sessionID);
            // console.log('req params ', req.params);
            // console.log('req ', req)
            req.session.test = 'TEST ' + req.params.id; //without any placement, session will always expire
            next();
        });
    }
}
exports.ExpressObj = ExpressObj;
//Example usage with ExpressObj
// const server = new ExpressObj();
// const route = new ExpressRoutes(server.app);
// route.GET = ((req, res)=>{
//     console.log(req.id);
// })
class ExpressRoutes {
    constructor(app, routeName = 'test', routeParam = ['id'], //parameters after the /api/test/[id]/....
    URL // override for routeName and routeParams ex. '/res'
    ) {
        this.app = app;
        this.routeName = routeName;
        this.routeParam = routeParam;
        this.URL = URL; // override for routeName and routeParams ex. '/res'
        this.disableAll = false; //disables .all, won't work with CORS for some reason
        if (!this.URL) {
            let routeParamBuilder = '';
            this.routeParam.forEach(r => {
                routeParamBuilder = routeParamBuilder + '/:' + r;
            });
            this.URL = '/api/' + routeName + routeParamBuilder;
        }
        this.Router = this.app.route(this.URL);
        console.log('Route URL: ', this.URL);
        if (!this.disableAll) {
        }
        this.init();
    }
    init() {
        this.setGet();
        this.setPost();
        this.setPut();
        this.setDelete();
    }
    setGet() {
        this.app.get(this.URL, (req, res) => {
            if (this.GET) {
                console.log('GET CALLED');
                console.log('session id GET ', req.sessionID);
                this.GET(req, res);
            }
            else {
                this.Undefine(res, 'GET');
            }
            //res.end() inside callback
        });
    }
    setPost() {
        this.app.post(this.URL, (req, res) => {
            if (this.POST) {
                console.log('POST CALLED');
                console.log('session id POST ', req.sessionID);
                this.POST(req, res);
            }
            else {
                this.Undefine(res, 'POST');
            }
            //res.end() inside callback
        });
    }
    setPut() {
        this.app.put((req, res) => {
            if (this.PUT) {
                console.log('PUT CALLED');
                this.PUT(req, res);
                //res.end() inside callback
            }
            else {
                this.Undefine(res, 'PUT');
            }
        });
    }
    setDelete() {
        this.app.delete((req, res) => {
            if (this.DELETE) {
                console.log('DELETE CALLED');
                this.DELETE(req, res);
                //res.end() inside callback
            }
            else {
                this.Undefine(res, 'DELETE');
            }
        });
    }
    Undefine(res, text) {
        console.log('UNDEFINE: ' + text);
        res.send('UNDEFINE: ' + text);
    }
}
exports.ExpressRoutes = ExpressRoutes;
// npm install socket.io-client
//creates a room for user, room name is the same as user name
//Define like this
// const e = new ExpressObj();
// const s = new ExpressSocket(e.io);  
//On the client, implement
//this.socket = io('http://localhost:1234')
//this.socket.emit('join', {user: 'user name'})
//this.socket.emit('info')
//this.socket.on('hasJoin', e=>{....
//this.socket.on('info', e=>{...
class ExpressSocket {
    constructor(io, delegates, room) {
        this.io = io;
        this.delegates = delegates;
        this.io.on('disconnect', () => console.log('IO has disconnected'));
        this.io.on('connect_error', () => console.log('IO has connect_error'));
        this.io.on('connection', s => {
            console.log('Has connected: ', s.id);
            console.log('Handshake ', s.handshake.query['id']);
            //Join room like this on the client.. this.socket.emit('join', {user: GUID.create(), room:'costRoom'})
            s.on('join', s0 => {
                if (s0.user) {
                    if (!room)
                        room = s0.user;
                    s.join(room);
                    s.emit('hasJoin', s0);
                    console.log('JOIN ', s0);
                }
                else {
                    console.log('Unable to join, no user define');
                }
            });
            s.on('info', s0 => {
                console.log('Requested info');
                s.emit('info', { idAndRoom: s.rooms });
            });
            //creates a reponse based on delegates list where "on" is the emitter trigger name
            //and delegate will reponse by a callback with the produced data
            //f is the value pass from external for the function func to use
            this.delegates.forEach(e => {
                console.log('registered: ', e.name);
                s.on(e.name, f => {
                    console.log('Got from delegate: ', e.name);
                    e.func(f, (r) => {
                        //emit to room
                        if (room) {
                            s.to(room).emit(e.name, r);
                        }
                        //emit to self
                        s.emit(e.name, r);
                    });
                });
            });
        });
    }
}
exports.ExpressSocket = ExpressSocket;
class Captcha {
    //remoteAddress is usually req.connection.remoteAddress
    constructor(serverSecret, captchaResponse, remoteAddress, callback) {
        this.serverSecret = serverSecret;
        this.captchaResponse = captchaResponse;
        this.remoteAddress = remoteAddress;
        this.verifyUrl = `https://google.com/recaptcha/api/siteverify?secret=${this.serverSecret}&response=${this.captchaResponse}&remoteip=${this.remoteAddress}`;
        request(this.verifyUrl, (err, response, body) => {
            body = JSON.parse(body);
            // console.log(body);
            // If Not Successful
            if (body.success !== undefined && !body.success) {
                console.log('captcha request has failed');
                callback(false);
                return;
            }
            //If Successful
            console.log('captcha request was successful');
            callback(true);
            return;
        });
    }
    static factory(serverSecret, req, callback) {
        //assume your req body has {captcha: ""....}
        new Captcha(serverSecret, req.body.captcha, req.connection.remoteAddress, callback);
    }
}
exports.Captcha = Captcha;
class CORSConfig {
    constructor(methods, selfSite, sessionExpire, sessionStore) {
        this.methods = methods;
        this.selfSite = selfSite;
        this.sessionExpire = sessionExpire;
        this.sessionStore = sessionStore;
        if (!methods)
            this.methods = 'GET, POST, OPTIONS, PUT, PATCH, DELETE';
        if (!selfSite)
            this.selfSite = '*';
        if (!sessionExpire)
            this.sessionExpire = 10000; //10 seconds
        if (!sessionStore)
            this.sessionStore = SessionStore.none;
    }
    static factory() {
        return new CORSConfig(null, null, null, null);
    }
}
exports.CORSConfig = CORSConfig;
var SessionStore;
(function (SessionStore) {
    SessionStore[SessionStore["none"] = 0] = "none";
    SessionStore[SessionStore["NonDB"] = 1] = "NonDB";
    SessionStore[SessionStore["MongoDB"] = 2] = "MongoDB";
})(SessionStore = exports.SessionStore || (exports.SessionStore = {}));
class SSLConfig {
    constructor(keyPath, certPath, passphrase) {
        this.key = fs.readFileSync(keyPath, 'utf8');
        this.cert = fs.readFileSync(certPath, 'utf8');
        this.passphrase = passphrase;
    }
}
exports.SSLConfig = SSLConfig;
//# sourceMappingURL=Webserver.js.map